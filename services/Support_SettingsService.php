<?php

namespace Craft;

class Support_SettingsService extends BaseApplicationComponent
{
    /** @var  BasePlugin */
    private $plugin;

    /**
     * Initialize
     */
    public function init()
    {
        $this->plugin = craft()->plugins->getPlugin('support');
    }

    /**
     * @param string $option
     *
     * @return mixed
     */
    public function getOption($option)
    {
        return $this->getSettings()->$option;
    }

    /**
     * Get all settings from plugin core class.
     *
     * @return Support_SettingsModel|BaseModel
     */
    public function getSettings()
    {
        $data = $this->plugin->getSettings();

        return Support_SettingsModel::populateModel($data);
    }

    /**
     * Set all settings from plugin core class.
     *
     * @param Support_SettingsModel|BaseModel|array $settings
     *
     * @return bool
     */
    public function saveSettings(Support_SettingsModel $settings)
    {
        if (! $settings->validate($settings))
        {
            $errors = $settings->getAllErrors();

            return false;
        }

        craft()->plugins->savePluginSettings($this->plugin, $settings);

        return true;
    }

    /**
     * Returns an array containing Asset Source models, indexed by their ids.
     *
     * @return AssetSourceModel[]
     */
    public function getAssetSources()
    {
        /** @var AssetSourceModel[] $assetSources */
        $assetSources = craft()->assetSources->getAllSources('id');

        $optionList = [];

        foreach ($assetSources as $id => $assetSource)
        {
            $optionList[$id] = $assetSource->name;
        }

        return $optionList;
    }

}
