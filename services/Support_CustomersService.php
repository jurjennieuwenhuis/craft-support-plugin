<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

class Support_CustomersService extends BaseApplicationComponent
{
    /**
     * Returns an customer by its ID.
     *
     * @param int $id
     * @return Support_CustomerModel|BaseElementModel|null
     */
    public function getCustomerById($id)
    {
        return craft()->elements->getElementById($id, 'Support_Customer');
    }

    /**
     * @param null $indexBy
     * @return Support_CustomerModel[]|array
     */
    public function getAllCustomers($indexBy = null)
    {
        $records = Support_CustomerRecord::model()->findAll([
            'order' => 'name ASC',
        ]);

        return Support_CustomerModel::populateModels($records, $indexBy);
    }

    /**
     * Returns an array of customer names, indexed by their id's.
     * Can be used in a <select> option list.
     *
     * @return array
     */
    public function getCustomerList()
    {
        $modelsById = $this->getAllCustomers('id');

        $optionList = [];

        foreach ($modelsById as $id => $customer)
        {
            $optionList[$id] = $customer->name;
        }

        return $optionList;
    }

    public function getOptionList()
    {
        return $this->getCustomerList();
    }

    /**
     * Saves an customer.
     *
     * @param Support_CustomerModel $customer
     * @return bool
     * @throws Exception
     * @throws \Exception
     */
    public function saveCustomer(Support_CustomerModel $customer)
    {
        $isNewCustomer = ! $customer->id;

        // Customer data
        if (! $isNewCustomer)
        {
            $record = Support_CustomerRecord::model()->findById($customer->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No customer exists with the ID "{id}"', ['id' => $customer->id]));
            }
        }
        else
        {
            $record = new Support_CustomerRecord();
        }

        // Fire an 'onBeforeSaveCustomer' event
        $event = new Event($this, [
            'customer' => $customer,
            'isNewCustomer' => $isNewCustomer,
        ]);

        $this->onBeforeSaveSupportCustomer($event);

        // Update the record
        $record->code = $customer->code;
        $record->name = $customer->name;

        // Validate the model
        $customer->validate();

        // Validate the record
        $record->validate();
        $customer->addErrors($record->getErrors());

        if ($customer->hasErrors())
        {
            return false;
        }

        SupportDbHelper::beginStackedTransaction();

        try
        {
            if ($event->performAction)
            {
                $success = craft()->elements->saveElement($customer);

                if ($success)
                {
                    // Now that we have an element ID, save it on the other stuff
                    if ($isNewCustomer)
                    {
                        $record->id = $customer->id;
                    }

                    $record->save(true);

                    SupportDbHelper::commitStackedTransaction();
                }
            }
            else
            {
                $success = false;
            }
        }
        catch (\Exception $e)
        {
            SupportDbHelper::rollbackStackedTransaction();

            throw $e;
        }

        if ($success)
        {
            // Fire an 'onSaveCustomer' event
            $this->onSaveSupportCustomer(new Event($this, [
                'customer' => $customer,
                'isNewCustomer' => $isNewCustomer,
            ]));
        }

        return $success;
    }

    /**
     * @param Support_CustomerModel $customer
     *
     * @return bool
     */
    public function deleteCustomer(Support_CustomerModel $customer)
    {
        return craft()->elements->deleteElementById($customer->id);
    }

    /**
     * Fires an 'onBeforeSaveCustomer' event.
     *
     * @param Event $event
     *
     * @throws \CException
     */
    public function onBeforeSaveSupportCustomer(Event $event)
    {
        $this->raiseEvent('onBeforeSaveSupportCustomer', $event);
    }

    /**
     * Fires an 'onSaveSupportCustomer' event.
     *
     * @param Event $event
     * @throws \CException
     */
    public function onSaveSupportCustomer(Event $event)
    {
        $this->raiseEvent('onSaveSupportCustomer', $event);
    }
}
