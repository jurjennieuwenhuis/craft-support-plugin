<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

/**
 * Issue status service.
 *
 * @package craft.plugins.support.services
 */
class Support_IssueStatusesService extends BaseApplicationComponent
{
    /**
     * @param string $handle
     *
     * @return Support_IssueStatusModel|BaseModel|null
     */
    public function getIssueStatusByHandle($handle)
    {
        $result = Support_IssueStatusRecord::model()->findByAttributes([
            'handle' => $handle,
        ]);

        if ($result)
        {
            return Support_IssueStatusModel::populateModel($result);
        }

        return null;
    }

    /**
     * Get the default issue status from the DB.
     *
     * @return Support_IssueStatusModel|BaseModel|null
     */
    public function getDefaultIssueStatus()
    {
        $result = Support_IssueStatusRecord::model()->findByAttributes([
            'default' => true,
        ]);

        if ($result)
        {
            return Support_IssueStatusModel::populateModel($result);
        }

        return null;
    }

    /**
     * @param Support_IssueStatusModel|BaseModel $model
     *
     * @return bool
     *
     * @throws Exception
     * @throws \Exception
     */
    public function saveIssueStatus(Support_IssueStatusModel $model)
    {
        if ($model->id)
        {
            /** @var Support_IssueStatusRecord $record */
            $record = Support_IssueStatusRecord::model()->findById($model->id);

            if (! $record->id)
            {
                throw new Exception(Craft::t('No issue status exists with the ID “{id}”', [
                    'id' => $model->id,
                ]));
            }
        }
        else
        {
            $record = new Support_IssueStatusRecord();
        }

        $record->name = $model->name;
        $record->handle = $model->handle;
        $record->color = $model->color;
        $record->sortOrder = $model->sortOrder;
        $record->default = $model->default;
        $record->closed = $model->closed;

        $record->validate();
        $model->addErrors($record->getErrors());

        // Saving ...
        if (! $model->hasErrors())
        {
            SupportDbHelper::beginStackedTransaction();

            try
            {
                if ($record->default)
                {
                    Support_IssueStatusRecord::model()->updateAll(['default' => 0]);
                }

                // Save it!
                $record->save(false);

                // Now that we have an id, save it to the model
                $model->id = $record->id;

                SupportDbHelper::commitStackedTransaction();
            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteIssueStatusById($id)
    {
        $issueStatuses = $this->getAllIssueStatuses();

        // Test if the issue status is used
        $criteria = craft()->elements->getCriteria('Support_Issue');
        $criteria->issueStatusId = $id;
        $issue = $criteria->first();

        if ($issue)
        {
            return false;
        }

        // Only delete the issue status if there's more than one.
        if (count($issueStatuses) >= 1)
        {
            Support_IssueStatusRecord::model()->deleteByPk($id);

            return true;
        }

        return false;
    }

    /**
     * @param array|\CDbCriteria $criteria
     *
     * @return Support_IssueStatusModel[]|BaseModel[]|array
     */
    public function getAllIssueStatuses($criteria = [])
    {
        $criteria['order'] = 'sortOrder ASC';
        $records = Support_IssueStatusRecord::model()->findAll($criteria);

        return Support_IssueStatusModel::populateModels($records);
    }

    /**
     * @return Support_IssueStatusModel[]|BaseModel[]|array
     */
    public function getOpenIssueStatuses($indexBy = null)
    {
        $ids = craft()->db->createCommand()
            ->select('id')
            ->from('support_issuestatuses')
            ->where('closed=:closed', [
                'closed' => false,
            ])
            ->queryColumn();

        return $ids;
    }

    /**
     * @return Support_IssueStatusModel[]|BaseModel[]|array
     */
    public function getClosedIssueStatuses()
    {
        $ids = craft()->db->createCommand()
            ->select('id')
            ->from('support_issuestatuses')
            ->where('closed=:closed', [
                'closed' => true,
            ])
            ->queryColumn();

        return $ids;
    }

    /**
     * @return array
     */
    public function getStatusNames()
    {
        $optionList = [];

        $statuses = $this->getAllIssueStatuses();

        foreach ($statuses as $status)
        {
            $optionList[$status->id] = $status->name;
        }

        return $optionList;
    }

    public function getOptionList()
    {
        return $this->getStatusNames();
    }

    /**
     * @param int $id
     *
     * @return Support_IssueStatusModel|null
     */
    public function getIssueStatusById($id)
    {
        $result = Support_IssueStatusRecord::model()->findById($id);

        if ($result)
        {
            return Support_IssueStatusModel::populateModel($result);
        }

        return null;
    }

    /**
     * @param array $ids
     *
     * @return bool
     */
    public function reorderIssueStatuses($ids)
    {
        foreach ($ids as $sortOrder => $id)
        {
            craft()->db->createCommand()->update('support_issuestatuses',
                ['sortOrder' => $sortOrder + 1],
                ['id' => $id]
            );
        }

        return true;
    }
}
