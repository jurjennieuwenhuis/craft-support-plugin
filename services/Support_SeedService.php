<?php
namespace Craft;

/**
 * Seed service.
 *
 * @author    Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @copyright Copyright (c) 2015, Pixel & Tonic, Inc.
 * @license   https://craftcommerce.com/license Craft Commerce License Agreement
 * @see       https://craftcommerce.com
 * @package   craft.plugins.commerce.services
 * @since     1.0
 */
class Support_SeedService extends BaseApplicationComponent
{
    /**
     * Default seeders
     */
    public function afterInstall()
    {
        $this->defaultCustomerSettings();
        $this->defaultProjectSettings();
        $this->defaultIssueSettings();
        $this->defaultCustomers();
        $this->defaultProjects();
        $this->defaultIssues();
    }

    private function defaultCustomerSettings()
    {
        $customerSettings = new Support_CustomerSettingsModel();
        $customerSettings->name = 'Customer';
        $customerSettings->handle = 'customer';

        // Set the field layout
        $fieldLayout = craft()->fields->assembleLayout([], []);
        $fieldLayout->type =  'Support_Customer';
        $customerSettings->setFieldLayout($fieldLayout);

        craft()->support_customerSettings->saveCustomerSetting($customerSettings);
    }

    private function defaultProjectSettings()
    {
        $settings = new Support_ProjectSettingsModel();
        $settings->name = 'Project';
        $settings->handle = 'project';

        // Set the field layout
        $fieldLayout = craft()->fields->assembleLayout([], []);
        $fieldLayout->type =  'Support_Project';
        $settings->setFieldLayout($fieldLayout);

        craft()->support_projectSettings->saveProjectSetting($settings);
    }

    private function defaultIssueSettings()
    {
        $issueSettings = new Support_IssueSettingsModel();
        $issueSettings->name = 'Issue';
        $issueSettings->handle = 'issue';

        $fieldLayout = craft()->fields->assembleLayout([], []);
        $fieldLayout->type = 'Support_Issue';
        $issueSettings->setFieldLayout($fieldLayout);

        craft()->support_issueSettings->saveIssueSetting($issueSettings);

        // Issue type

        $data = [
            'name' => 'Bug',
            'handle' => 'bug',
            'default' => true,
        ];

        $defaultType = Support_IssueTypeModel::populateModel($data);
        craft()->support_issueTypes->saveIssueType($defaultType);

        $data = [
            'name' => 'Change Request',
            'handle' => 'change_request',
            'default' => false,
        ];

        $type = Support_IssueTypeModel::populateModel($data);
        craft()->support_issueTypes->saveIssueType($type);


        // Issue status

        craft()->support_issueStatuses->saveIssueStatus(Support_IssueStatusModel::populateModel([
            'name' => 'New',
            'handle' => 'new',
            'color' => 'yellow',
            'default' => true,
            'closed' => false,
        ]));

        craft()->support_issueStatuses->saveIssueStatus(Support_IssueStatusModel::populateModel([
            'name' => 'Open',
            'handle' => 'open',
            'color' => 'green',
            'default' => false,
            'closed' => false,
        ]));

        craft()->support_issueStatuses->saveIssueStatus(Support_IssueStatusModel::populateModel([
            'name' => 'Feedback',
            'handle' => 'feedback',
            'color' => 'pink',
            'default' => false,
            'closed' => false,
        ]));

        craft()->support_issueStatuses->saveIssueStatus(Support_IssueStatusModel::populateModel([
            'name' => 'Closed',
            'handle' => 'closed',
            'color' => 'red',
            'default' => false,
            'closed' => true,
        ]));
    }

    private function defaultCustomers()
    {
        $customer = Support_CustomerModel::populateModel([
            'name' => 'Acme Inc.',
            'code' => 'ACM',
        ]);

        craft()->support_customers->saveCustomer($customer);
    }

    private function defaultProjects()
    {
        $customers = craft()->support_customers->getAllCustomers();

        $project = Support_ProjectModel::populateModel([
            'name' => 'Acme Maintenance.',
            'code' => 'ACM-01',
            'customerId' => $customers[0]->id,
        ]);

        craft()->support_projects->saveProject($project);
    }

    private function defaultIssues()
    {
        $projects = craft()->support_projects->getAllProjects();
        $defaultIssueType = craft()->support_issueTypes->getDefaultIssueType();

        // New issue

        /** @var Support_IssueModel $issue */
        $issue = Support_IssueModel::populateModel([
            'description' => '<p>Lorem Ipsum</p>',
            'projectId'  => $projects[0]->id,
            'issueStatusId' => craft()->support_issueStatuses->getIssueStatusByHandle('new')->id,
            'issueTypeId' => $defaultIssueType->id,
        ]);

        $issue->getContent()->title = 'New issue';

        craft()->support_issues->saveIssue($issue);

        // Open issue

        /** @var Support_IssueModel $issue */
        $issue = Support_IssueModel::populateModel([
            'description' => '<p>Lorem Ipsum</p>',
            'projectId'  => $projects[0]->id,
            'issueStatusId' => craft()->support_issueStatuses->getIssueStatusByHandle('open')->id,
            'issueTypeId' => $defaultIssueType->id,
        ]);

        $issue->getContent()->title = 'Open issue';

        craft()->support_issues->saveIssue($issue);

        // Feedback issue

        /** @var Support_IssueModel $issue */
        $issue = Support_IssueModel::populateModel([
            'description' => '<p>Lorem Ipsum</p>',
            'projectId'  => $projects[0]->id,
            'issueStatusId' => craft()->support_issueStatuses->getIssueStatusByHandle('feedback')->id,
            'issueTypeId' => $defaultIssueType->id,
        ]);

        $issue->getContent()->title = 'Feedback issue';

        craft()->support_issues->saveIssue($issue);

        // Closed issue

        /** @var Support_IssueModel $issue */
        $issue = Support_IssueModel::populateModel([
            'description' => '<p>Lorem Ipsum</p>',
            'projectId'  => $projects[0]->id,
            'issueStatusId' => craft()->support_issueStatuses->getIssueStatusByHandle('closed')->id,
            'issueTypeId' => $defaultIssueType->id,
        ]);

        $issue->getContent()->title = 'Closed issue';

        craft()->support_issues->saveIssue($issue);
    }

}
