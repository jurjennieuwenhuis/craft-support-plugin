<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

class Support_IssueSettingsService extends BaseApplicationComponent
{
    /**
     * @var array
     */
    private $issueSettingsById = [];

    /**
     * @param int $issueSettingsId
     *
     * @return Support_IssueSettingsModel|null
     */
    public function getIssueSettingById($issueSettingsId)
    {
        if (! isset($this->issueSettingsById) || ! array_key_exists($issueSettingsId, $this->issueSettingsById))
        {
            $result = Support_IssueSettingsRecord::model()->findById($issueSettingsId);

            if ($result)
            {
                $issueSetting = Support_IssueSettingsModel::populateModel($result);
            }
            else
            {
                $issueSetting = null;
            }

            $this->issueSettingsById[$issueSettingsId] = $issueSetting;
        }

        if (isset($this->issueSettingsById[$issueSettingsId]))
        {
            return $this->issueSettingsById[$issueSettingsId];
        }

        return null;
    }

    /**
     * Returns the issue settings model by its handle.
     *
     * @param string $handle
     * @return Support_IssueSettingsModel|null
     */
    public function getIssueSettingByHandle($handle)
    {
        $result = Support_IssueSettingsRecord::model()->findByAttributes([
            'handle' => $handle,
        ]);

        if ($result)
        {
            $issueSetting = Support_IssueSettingsModel::populateModel($result);
            $this->issueSettingsById[$issueSetting->id] = $issueSetting;

            return $issueSetting;
        }

        return null;
    }

    /**
     * Saves an issue type.
     *
     * @param Support_IssueSettingsModel $issueSetting
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \CDbException
     */
    public function saveIssueSetting(Support_IssueSettingsModel $issueSetting)
    {
        if ($issueSetting->id)
        {
            $issueSettingsRecord = Support_IssueSettingsRecord::model()->findById($issueSetting->id);

            if (! $issueSettingsRecord)
            {
                throw new Exception(Craft::t('No issue settings exists with the ID “{id}”', [
                    'id' => $issueSetting->id
                ]));
            }

            $oldIssueSetting = Support_IssueSettingsModel::populateModel($issueSettingsRecord);
            $isNewIssueSetting = false;
        }
        else
        {
            $issueSettingsRecord = new Support_IssueSettingsRecord();
            $isNewIssueSetting = true;
        }

        $issueSettingsRecord->name = $issueSetting->name;
        $issueSettingsRecord->handle = $issueSetting->handle;

        $issueSettingsRecord->validate();
        $issueSetting->addErrors($issueSettingsRecord->getErrors());

        if (! $issueSetting->hasErrors())
        {
            SupportDbHelper::beginStackedTransaction();

            try
            {
                if (! $isNewIssueSetting && $oldIssueSetting->fieldLayoutId)
                {
                    // Drop the old field layout
                    craft()->fields->deleteLayoutById($oldIssueSetting->fieldLayoutId);
                }

                // Save the new one
                $fieldLayout = $issueSetting->getFieldLayout();
                craft()->fields->saveLayout($fieldLayout);

                // Update the issue type record/model with the new layout ID
                $issueSetting->fieldLayoutId = $fieldLayout->id;
                $issueSettingsRecord->fieldLayoutId = $fieldLayout->id;

                // Save it
                $issueSettingsRecord->save(false);

                // Now that we have a issue settings ID, save it on the model
                if (! $issueSetting->id)
                {
                    $issueSetting->id = $issueSettingsRecord->id;
                }

                // Might as well update our cache of the calendar while we have it.
                $this->issueSettingsById[$issueSetting->id] = $issueSetting;

                SupportDbHelper::commitStackedTransaction();
            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
