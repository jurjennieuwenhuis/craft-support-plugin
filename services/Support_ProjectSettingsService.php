<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

class Support_ProjectSettingsService extends BaseApplicationComponent
{
    private $projectSettingsById = [];

    /**
     * Returns the project settings by its ID.
     *
     * @param int $id
     * @return Support_ProjectSettingsModel|null
     */
    public function getProjectSettingById($id)
    {
        if (! isset($this->projectSettingsById) || ! array_key_exists($id, $this->projectSettingsById))
        {
            $result = Support_ProjectSettingsRecord::model()->findById($id);

            if ($result)
            {
                $this->projectSettingsById[$id] = Support_ProjectSettingsModel::populateModel($result);
            }
            else
            {
                $this->projectSettingsById[$id] = null;
            }
        }

        if (isset($this->projectSettingsById[$id]))
        {
            return $this->projectSettingsById[$id];
        }

        return null;
    }

    /**
     * Return the projects settings by its handle.
     *
     * @param string $handle
     * @return Support_ProjectSettingsModel|null
     */
    public function getProjectSettingByHandle($handle)
    {
        $result = Support_ProjectSettingsRecord::model()->findByAttributes([
            'handle' => $handle,
        ]);

        if ($result)
        {
            $projectSetting = Support_ProjectSettingsModel::populateModel($result);
            $this->projectSettingsById[$projectSetting->id] = $projectSetting;

            return $projectSetting;
        }

        return null;
    }

    /**
     * Saves the project settings.
     *
     * @param Support_ProjectSettingsModel $projectSetting
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \CDbException
     */
    public function saveProjectSetting(Support_ProjectSettingsModel $projectSetting)
    {
        if ($projectSetting->id)
        {
            $record = Support_ProjectSettingsRecord::model()->findById($projectSetting->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No project settings exists with the ID "{id}"', [
                    'id' => $projectSetting->id
                ]));
            }

            $oldProjectSetting = Support_ProjectSettingsModel::populateModel($record);
            $isNewProjectSetting = false;
        }
        else
        {
            $record = new Support_ProjectSettingsRecord();
            $isNewProjectSetting = true;
        }

        $record->name = $projectSetting->name;
        $record->handle = $projectSetting->handle;

        $record->validate();
        $projectSetting->addErrors($record->getErrors());

        if (! $projectSetting->hasErrors())
        {
            SupportDbHelper::beginStackedTransaction();

            try
            {
                if (! $isNewProjectSetting && $oldProjectSetting->fieldLayoutId)
                {
                    // Drop the old field layout
                    craft()->fields->deleteLayoutById($oldProjectSetting->fieldLayoutId);
                }

                // Save the new one
                $fieldLayout = $projectSetting->getFieldLayout();
                craft()->fields->saveLayout($fieldLayout);

                // Update the issue type record/model with the new layout ID
                $projectSetting->fieldLayoutId = $fieldLayout->id;
                $record->fieldLayoutId = $fieldLayout->id;

                // Save it
                $record->save(false);

                // Now that we have a issue type ID, save it on the model
                if (! $projectSetting->id)
                {
                    $projectSetting->id = $record->id;
                }

                // Might as well update our cache of the calendar while we have it.
                $this->projectSettingsById[$projectSetting->id] = $projectSetting;

                SupportDbHelper::commitStackedTransaction();
            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
