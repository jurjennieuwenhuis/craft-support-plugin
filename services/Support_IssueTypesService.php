<?php

namespace Craft;
use Support\Helper\SupportDbHelper;

/**
 * Issue type service.
 *
 * @package craft.plugins.support.services
 */
class Support_IssueTypesService extends BaseApplicationComponent
{
    /**
     * @param string $handle
     *
     * @return Support_IssueTypeModel|BaseModel|null
     */
    public function getIssueTypeByHandle($handle)
    {
        $result = Support_IssueTypeRecord::model()->findByAttributes([
            'handle' => $handle,
        ]);

        if ($result)
        {
            return Support_IssueTypeModel::populateModel($result);
        }

        return null;
    }

    /**
     * Get the default issue type from the DB.
     *
     * @return Support_IssueTypeModel|BaseModel|null
     */
    public function getDefaultIssueType()
    {
        $result = Support_IssueTypeRecord::model()->findByAttributes([
            'default' => true,
        ]);

        if ($result)
        {
            return Support_IssueTypeModel::populateModel($result);
        }

        return null;
    }

    /**
     * @param Support_IssueTypeModel|BaseModel $issueType
     *
     * @return bool
     *
     * @throws Exception
     * @throws \Exception
     */
    public function saveIssueType(Support_IssueTypeModel $issueType)
    {
        if ($issueType->id)
        {
            $record = Support_IssueTypeRecord::model()->findById($issueType->id);

            if (! $record->id)
            {
                throw new Exception(Craft::t('No issue type exists with the ID “{id}”', [
                    'id' => $issueType->id,
                ]));
            }
        }
        else
        {
            $record = new Support_IssueTypeRecord();
        }

        $record->name = $issueType->name;
        $record->handle = $issueType->handle;
        $record->sortOrder = $issueType->sortOrder;
        $record->default = $issueType->default;

        $record->validate();
        $issueType->addErrors($record->getErrors());

        // Saving ...
        if (! $issueType->hasErrors())
        {
            SupportDbHelper::beginStackedTransaction();

            try
            {
                // only one issue type can be among issue types of one issue
                if ($record->default)
                {
                    Support_IssueTypeRecord::model()->updateAll(['default' => 0]);
                }

                // Save it!
                $record->save(false);

                // Now that we have an id, save it to the model
                $issueType->id = $record->id;

                SupportDbHelper::commitStackedTransaction();
            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteIssueTypeById($id)
    {
        $issueTypes = $this->getAllIssueTypes();

        // Test if the issue type is used
        $criteria = craft()->elements->getCriteria('Support_Issue');
        $criteria->issueTypeId = $id;
        $issue = $criteria->first();

        if ($issue)
        {
            return false;
        }

        // Only delete the issue type if there's more than one.
        if (count($issueTypes) >= 1)
        {
            Support_IssueTypeRecord::model()->deleteByPk($id);

            return true;
        }

        return false;
    }

    /**
     * @param array|\CDbCriteria $criteria
     *
     * @return array
     */
    public function getAllIssueTypes($criteria = [])
    {
        $criteria['order'] = 'sortOrder ASC';
        $records = Support_IssueTypeRecord::model()->findAll($criteria);

        return Support_IssueTypeModel::populateModels($records);
    }

    public function getOptionList()
    {
        $issueTypes = $this->getAllIssueTypes();

        $options = [];

        foreach ($issueTypes as $issueType)
        {
            $options[$issueType->id] = $issueType->name;
        }

        return $options;
    }

    /**
     * @param int $id
     *
     * @return Support_IssueTypeModel|null
     */
    public function getIssueTypeById($id)
    {
        $result = Support_IssueTypeRecord::model()->findById($id);

        if ($result)
        {
            return Support_IssueTypeModel::populateModel($result);
        }

        return null;
    }

    /**
     * @param array $ids
     *
     * @return bool
     */
    public function reorderIssueTypes($ids)
    {
        foreach ($ids as $sortOrder => $id)
        {
            craft()->db->createCommand()->update('support_issuetypes',
                ['sortOrder' => $sortOrder + 1],
                ['id' => $id]
            );
        }

        return true;
    }
}
