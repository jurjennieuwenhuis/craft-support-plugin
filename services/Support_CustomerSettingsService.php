<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

class Support_CustomerSettingsService extends BaseApplicationComponent
{
    private $customerSettingsById = [];

    /**
     * Returns the customer settings by its ID.
     *
     * @param int $id
     * @return Support_CustomerSettingsModel|null
     */
    public function getCustomerSettingById($id)
    {
        if (! isset($this->customerSettingsById) || ! array_key_exists($id, $this->customerSettingsById))
        {
            $result = Support_CustomerSettingsRecord::model()->findById($id);

            if ($result)
            {
                $this->customerSettingsById[$id] = Support_CustomerSettingsModel::populateModel($result);
            }
            else
            {
                $this->customerSettingsById[$id] = null;
            }
        }

        if (isset($this->customerSettingsById[$id]))
        {
            return $this->customerSettingsById[$id];
        }

        return null;
    }

    /**
     * Return the customers settings by its handle.
     *
     * @param string $handle
     * @return Support_CustomerSettingsModel|null
     */
    public function getCustomerSettingByHandle($handle)
    {
        $result = Support_CustomerSettingsRecord::model()->findByAttributes([
            'handle' => $handle,
        ]);

        if ($result)
        {
            $customerSetting = Support_CustomerSettingsModel::populateModel($result);
            $this->customerSettingsById[$customerSetting->id] = $customerSetting;

            return $customerSetting;
        }

        return null;
    }

    /**
     * Saves the customer settings.
     *
     * @param Support_CustomerSettingsModel $customerSetting
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \CDbException
     */
    public function saveCustomerSetting(Support_CustomerSettingsModel $customerSetting)
    {
        if ($customerSetting->id)
        {
            $record = Support_CustomerSettingsRecord::model()->findById($customerSetting->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No customer settings exists with the ID "{id}"', [
                    'id' => $customerSetting->id
                ]));
            }

            $oldCustomerSetting = Support_CustomerSettingsModel::populateModel($record);
            $isNewCustomerSetting = false;
        }
        else
        {
            $record = new Support_CustomerSettingsRecord();
            $isNewCustomerSetting = true;
        }

        $record->name = $customerSetting->name;
        $record->handle = $customerSetting->handle;

        $record->validate();
        $customerSetting->addErrors($record->getErrors());

        if (! $customerSetting->hasErrors())
        {
            SupportDbHelper::beginStackedTransaction();

            try
            {
                if (! $isNewCustomerSetting && $oldCustomerSetting->fieldLayoutId)
                {
                    // Drop the old field layout
                    craft()->fields->deleteLayoutById($oldCustomerSetting->fieldLayoutId);
                }

                // Save the new one
                $fieldLayout = $customerSetting->getFieldLayout();
                craft()->fields->saveLayout($fieldLayout);

                // Update the issue type record/model with the new layout ID
                $customerSetting->fieldLayoutId = $fieldLayout->id;
                $record->fieldLayoutId = $fieldLayout->id;

                // Save it
                $record->save(false);

                // Now that we have a issue type ID, save it on the model
                if (! $customerSetting->id)
                {
                    $customerSetting->id = $record->id;
                }

                // Might as well update our cache of the calendar while we have it.
                $this->customerSettingsById[$customerSetting->id] = $customerSetting;

                SupportDbHelper::commitStackedTransaction();
            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
