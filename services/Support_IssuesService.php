<?php

namespace Craft;

use Support\Helper\SupportDbHelper;

class Support_IssuesService extends BaseApplicationComponent
{
    /**
     * Returns an issue by its ID.
     *
     * @param int $id
     * @return Support_IssueModel|BaseElementModel|null
     */
    public function getIssueById($id)
    {
        return craft()->elements->getElementById($id, 'Support_Issue');
    }

    /**
     * @param Support_ProjectModel $project
     *
     * @return Support_IssueModel[]|array|null
     */
    public function getIssuesByProject($project)
    {
        $criteria = craft()->elements->getCriteria('Support_Issue');
        $criteria->project = $project;
        $criteria->limit = null;

        return $criteria->find();
    }

    /**
     * @param Support_CustomerModel $customer
     *
     * @return Support_CustomerModel[]|array
     */
    public function getIssuesByCustomer($customer)
    {
        $criteria = craft()->elements->getCriteria('Support_Issue');
        $criteria->customer = $customer;
        $criteria->limit = null;

        return $criteria->find();
    }

    /**
     * Saves an issue.
     *
     * @param Support_IssueModel|BaseModel $issue
     * @return bool
     * @throws Exception
     * @throws \Exception
     */
    public function saveIssue(Support_IssueModel $issue)
    {
        $isNewIssue = ! $issue->id;

        // Issue data
        if (! $isNewIssue)
        {
            $record = Support_IssueRecord::model()->findById($issue->id);

            if (! $record)
            {
                throw new Exception(Craft::t('No issue exists with the ID "{id}"', ['id' => $issue->id]));
            }
        }
        else
        {
            $record = new Support_IssueRecord();
        }

        // Retrieve any asset ids
        $this->getAssetIds($issue);

        // Fire an 'onBeforeSaveSupportIssue' event
        $event = new Event($this, [
            'issue' => $issue,
            'isNewIssue' => $isNewIssue,
        ]);

        $this->onBeforeSaveSupportIssue($event);

        // Update the record
        $record->issueStatus    = $issue->issueStatus;
        $record->projectId      = $issue->projectId;
        $record->issueTypeId    = $issue->issueTypeId;
        $record->issueStatusId  = $issue->issueStatusId;
        $record->description    = $issue->description;
        $record->solution       = $issue->solution;

        // Validate the model
        $issue->validate();

        // Validate the record
        $record->validate();
        $issue->addErrors($record->getErrors());

        if ($issue->hasErrors())
        {
            return false;
        }

        SupportDbHelper::beginStackedTransaction();


        try
        {
            if ($event->performAction)
            {
                $success = craft()->elements->saveElement($issue);

                if ($success)
                {
                    // Now that we have an element ID, save it on the other stuff
                    if ($isNewIssue)
                    {
                        $record->id = $issue->id;
                    }

                    $record->save(true);

                    $this->saveFiles($issue);

                    SupportDbHelper::commitStackedTransaction();
                }
            }
            else
            {
                $success = false;
            }
        }
        catch (\Exception $e)
        {
            SupportDbHelper::rollbackStackedTransaction();

            throw $e;
        }

        if ($success)
        {
            // Fire an 'onSaveSupportIssue' event
            $this->onSaveSupportIssue(new Event($this, [
                'issue' => $issue,
                'isNewIssue' => $isNewIssue,
            ]));
        }

        return $success;
    }

    /**
     * @param Support_IssueModel $issue
     *
     * @return bool
     */
    public function delete($issue)
    {
        return craft()->elements->deleteElementById($issue->id);
    }

    public function enableIssuesByProject(Support_ProjectModel $project, $enable)
    {
        $issues = $this->getIssuesByProject($project);

        if (! $issues)
        {
            return;
        }

        SupportDbHelper::beginStackedTransaction();

        try
        {
            foreach ($issues as $issue)
            {
                if ($issue->enabled !== $enable)
                {
                    $issue->enabled = $enable;
                    craft()->elements->saveElement($issue);
                }
            }

            SupportDbHelper::commitStackedTransaction();
        }
        catch (\Exception $e)
        {
            SupportDbHelper::rollbackStackedTransaction();

            throw $e;
        }
    }

    /**
     * @param Support_IssueModel $issue
     */
    public function getAssetIds($issue)
    {
        if (! $issue->id)
        {
            $issue->assetIds = [];
            return;
        }

        $assetIds = craft()->db->createCommand()
            ->select('assetId')
            ->from('support_issue_files')
            ->where('support_issue_files.issueId=:issueId', ['issueId' => $issue->id])
            ->queryColumn();

        $issue->assetIds = $assetIds;
    }

    /**
     * Returns a element criteria model with the related files.
     *
     * @param int $issueId
     *
     * @return ElementCriteriaModel
     */
    public function getFiles($issueId)
    {
        $criteria = \Craft\craft()->elements->getCriteria(ElementType::Asset);

        $assetIds = \Craft\craft()->db->createCommand()
            ->select('assetId')
            ->from('support_issue_files')
            ->where('support_issue_files.issueId=:issueId', ['issueId' => $issueId])
            ->queryColumn();

        $criteria->id = $assetIds;

        return $criteria;
    }

    /**
     * Saves the related assets and returns the number of affected rows.
     *
     * @param Support_IssueModel $issue
     * @return int
     */
    public function saveFiles($issue)
    {
        $assetIds = $issue->assetIds;

        // Firstly remove all related assets ids
        \Craft\craft()->db->createCommand()
            ->delete('support_issue_files', 'issueId=:issueId', ['issueId' => $issue->id]);

        if (null == $assetIds)
        {
            return 0;
        }

        $assetIds = (array) $assetIds;

        $rows = [];
        foreach ($assetIds as $assetId)
        {
            $rows[] = [$issue->id, $assetId];
        }

        $affected = \Craft\craft()->db->createCommand()
            ->insertAll('support_issue_files', ['issueId', 'assetId'], $rows);

        return $affected;
    }

    /**
     * Fires an 'onBeforeSaveIssue' event.
     *
     * @param Event $event
     *
     * @throws \CException
     */
    public function onBeforeSaveSupportIssue(Event $event)
    {
        $this->raiseEvent('onBeforeSaveSupportIssue', $event);
    }

    /**
     * Fires an 'onSaveSupportIssue' event.
     *
     * @param Event $event
     * @throws \CException
     */
    public function onSaveSupportIssue(Event $event)
    {
        $this->raiseEvent('onSaveSupportIssue', $event);
    }

    /**
     * Enables or disables issues, based on the project status.
     *
     * @param Event $event
     * @throws \Exception
     */
    public function projectStatusChangeHandler(Event $event)
    {
        $isNewProject = $event->params['isNewProject'];

        if (! $isNewProject)
        {
            $project = $event->params['project'];
            $oldProject = $event->params['oldProject'];

            if ($project->enabled !== $oldProject->enabled)
            {
                $this->enableIssuesByProject($project, $project->enabled);
            }
       }
    }
}
