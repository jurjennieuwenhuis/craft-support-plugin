<?php

namespace Craft;
use Support\Helper\SupportCustomerHelper;

/**
 * Class Support_CustomerElementType
 *
 * @package Craft
 */
class Support_CustomerElementType extends BaseElementType
{
    /**
     * Returns the element type name.
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Customers');
    }

    /**
     * @inheritDoc
     */
    public function hasStatuses()
    {
        return false;
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function hasTitles()
    {
        return false;
    }

    /**
     * @param null $source
     *
     * @return array
     */
    public function getAvailableActions($source = null)
    {
        $actions = [];

        if (craft()->userSession->checkPermission('support-manageCustomers'))
        {
            $deleteAction = craft()->elements->getAction('Delete');

            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected customers and their associated issues?'),
                'successMessage' => Craft::t('Customers deleted.'),
            ]);

            $actions[] = $deleteAction;
        }

        return $actions;
    }

    /**
     * Returns this element type's sources.
     *
     * @param string|null $context
     * @return array|false
     */
    public function getSources($context = null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Customers'),
                'defaultSort' => ['dateCreated', 'asc']
            ],
        ];

        return $sources;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @return array
     */
    public function defineAvailableTableAttributes()
    {
        return [
            'name' => Craft::t('Name'),
            'code' => Craft::t('Code'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function defineSearchableAttributes()
    {
        return ['name', 'code'];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultTableAttributes($source = null)
    {
        $attributes = [];

        $attributes[] = 'name';
        $attributes[] = 'code';
        $attributes[] = 'dateCreated';

        return $attributes;
    }


    /**
     * Returns the table view HTML for a given attribute.
     *
     * @param Support_CustomerModel|BaseElementModel $element
     * @param string $attribute
     * @return mixed|string
     */
    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'code':
            {
                $code = $element->code;

                if ($code)
                {
                    // Todo: return human readable name of status
                    return $code;
                }
                else
                {
                    return '';
                }
            }

            case 'name':
            {
                return $element->name;
            }

            default:
            {
                return parent::getTableAttributeHtml($element, $attribute);
            }
        }
    }

	/**
     * Defines any custom element criteria attributes for this element type.
     *
     * @return array
     */
    public function defineCriteriaAttributes()
    {
        return [
            'name'   => AttributeType::String,
            'code'           => AttributeType::Handle,
            'order'          => [AttributeType::String, 'default' => 'support_customers.name asc'],
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand $query
     * @param ElementCriteriaModel|Support_CustomerModel $criteria
     * @return false|null|void
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect(
                'support_customers.code,
                support_customers.name')
            ->join(
                'support_customers support_customers',
                'support_customers.id = elements.id');

        if ($criteria->name)
        {
            $query->andWhere(DbHelper::parseParam('support_customers.name', $criteria->name, $query->params));
        }

        if ($criteria->code)
        {
            $query->andWhere(DbHelper::parseParam('support_customers.code', $criteria->code, $query->params));
        }

        return null;
    }

    /**
     * Populates an element model based on query result.
     *
     * @param array $row
     * @return Support_CustomerModel|BaseModel|array
     */
    public function populateElementModel($row)
    {
        return Support_CustomerModel::populateModel($row);
    }

    /**
     * Returns the HTML for an editor HUD for the given element.
     *
     * @param BaseElementModel|Support_CustomerModel $element
     * @return string The HTML for the editor HUD.
     */
    public function getEditorHtml(BaseElementModel $element)
    {
        $templatesService = craft()->templates;
        $html = '';
        $html .= $templatesService->renderMacro('support/customers/_fields', 'nameField', [$element]);
        $html .= $templatesService->renderMacro('support/customers/_fields', 'codeField', [$element]);
        $html .= parent::getEditorHtml($element);

        return $html;
    }

    /**
     * @param Support_CustomerModel|BaseElementModel $element
     * @param array $params
     * @return bool
     */
    public function saveElement(BaseElementModel $element, $params)
    {
        SupportCustomerHelper::populateCustomerModel($element, $params);

        return craft()->support_customers->saveCustomer($element);
    }
}
