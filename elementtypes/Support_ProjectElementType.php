<?php

namespace Craft;
use Support\Helper\SupportProjectHelper;

/**
 * Class Support_ProjectElementType
 *
 * @package Craft
 */
class Support_ProjectElementType extends BaseElementType
{
    /**
     * Returns the element type name.
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Projects');
    }

    /**
     * @inheritDoc
     */
    public function hasStatuses()
    {
        return true;
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function hasTitles()
    {
        return false;
    }

    /**
     * @param null $source
     *
     * @return array
     */
    public function getAvailableActions($source = null)
    {
        $actions = [];

        if (craft()->userSession->checkPermission('support-manageProjects'))
        {
            $deleteAction = craft()->elements->getAction('Delete');

            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected projects and their associated issues?'),
                'successMessage' => Craft::t('Projects deleted.'),
            ]);

            $actions[] = $deleteAction;

            $enableAction = craft()->elements->getAction('Support_EnableProject');

            $enableAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to enable the selected projects and their associated issues?'),
                'successMessage' => Craft::t('Projects enabled.'),
            ]);

            $actions[] = $enableAction;

            $disableAction = craft()->elements->getAction('Support_DisableProject');

            $disableAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to disable the selected projects and their associated issues?'),
                'successMessage' => Craft::t('Projects disabled.'),
            ]);

            $actions[] = $disableAction;
        }

        return $actions;
    }

    /**
     * Returns this element type's sources.
     *
     * @param string|null $context
     * @return array|false
     */
    public function getSources($context = null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Projects'),
                'defaultSort' => ['dateCreated', 'asc']
            ],
        ];

        return $sources;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @return array
     */
    public function defineAvailableTableAttributes()
    {
        return [
            'name' => Craft::t('Name'),
            'code' => Craft::t('Code'),
            'favorite' => ['label' => Craft::t('Is Favorite?')],
            'customer' => ['label' => Craft::t('Customer')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function defineSearchableAttributes()
    {
        return ['name', 'code', 'customer'];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultTableAttributes($source = null)
    {
        $attributes = [];

        $attributes[] = 'name';
        $attributes[] = 'code';
        $attributes[] = 'customer';

        return $attributes;
    }

    /**
     * @inheritDoc
     */
    public function defineSortableAttributes()
    {
        $attributes = [
            'name'        => Craft::t('Name'),
            'code'        => Craft::t('Code'),
            'customerId'  => Craft::t('Customer'),
        ];

        return $attributes;
    }

    /**
     * Returns the table view HTML for a given attribute.
     *
     * @param Support_ProjectModel|BaseElementModel $element
     * @param string $attribute
     * @return mixed|string
     */
    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'code':
            {
                $code = $element->code;

                if ($code)
                {
                    // Todo: return human readable name of status
                    return $code;
                }
                else
                {
                    return '';
                }
            }

            case 'name':
            {
                return $element->name;
            }


            case 'customer':
            {
                $customer = $element->getCustomer();

                return ($customer ? $customer->name : '');
            }

            case 'customerId':
            {
                $customerId = $element->customerId;

                if ((int) $customerId > 0)
                {
                    return craft()->support_customers->getCustomerById($customerId);
                }
                else
                {
                    return '';
                }
            }

            case 'favorite':
            {
                if ($element->favorite)
                {
                    return '<span data-icon="check" title="' . Craft::t('Yes') . '"></span>';
                }
                else
                {
                    return '';
                }
            }

            default:
            {
                return parent::getTableAttributeHtml($element, $attribute);
            }
        }
    }

	/**
     * Defines any custom element criteria attributes for this element type.
     *
     * @return array
     */
    public function defineCriteriaAttributes()
    {
        return [
            'customer'   => AttributeType::Mixed,
            'customerId' => AttributeType::Mixed,
            'isFavorite' => AttributeType::Bool,
            'favorite'   => AttributeType::Mixed,
            'name'       => AttributeType::String,
            'code'       => AttributeType::String,
            'order'      => [AttributeType::String, 'default' => 'support_projects.name asc'],
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand $query
     * @param ElementCriteriaModel|Support_ProjectModel $criteria
     * @return false|null|void
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect(
                'support_projects.code,
                support_projects.customerId,
                support_projects.favorite,
                support_projects.name')
            ->join(
                'support_projects support_projects',
                'support_projects.id = elements.id');

        if ($criteria->name)
        {
            $query->andWhere(DbHelper::parseParam('support_projects.name', $criteria->name, $query->params));
        }

        if ($criteria->code)
        {
            $query->andWhere(DbHelper::parseParam('support_projects.code', $criteria->code, $query->params));
        }

        if ($criteria->customer)
        {
            if ($criteria->customer instanceof Support_CustomerModel)
            {
                $criteria->customerId = $criteria->customer->id;
                $criteria->customer = null;
            }
            else
            {
                $query->andWhere(DbHelper::parseParam('support_customers.id', $criteria->customer, $query->params));
            }
        }

        if ($criteria->customerId)
        {
            $query->andWhere(DbHelper::parseParam('support_projects.customerId', $criteria->customerId, $query->params));
        }

        if ($criteria->favorite)
        {
            if ($criteria->favorite == true)
            {
                $query->andWhere('support_projects.favorite=:favorite', [
                    'favorite' => true,
                ]);
            }
        }

        if ($criteria->isFavorite == true)
        {
            $query->andWhere('support_projects.favorite=:favorite', [
                'favorite' => true,
            ]);
        }

        return null;
    }

    /**
     * Populates an element model based on query result.
     *
     * @param array $row
     * @return Support_ProjectModel|BaseModel|array
     */
    public function populateElementModel($row)
    {
        return Support_ProjectModel::populateModel($row);
    }

    /**
     * Returns the HTML for an editor HUD for the given element.
     *
     * @param BaseElementModel|Support_ProjectModel $element
     * @return string The HTML for the editor HUD.
     */
    public function getEditorHtml(BaseElementModel $element)
    {
        $templatesService = craft()->templates;
        $html = '';
        $html .= $templatesService->renderMacro('support/projects/_fields', 'nameField', [$element]);
        $html .= $templatesService->renderMacro('support/projects/_fields', 'codeField', [$element]);
        $html .= $templatesService->renderMacro('support/projects/_fields', 'favoriteField', [$element]);
        $html .= $templatesService->renderMacro('support/projects/_fields', 'customerField', [$element]);
        $html .= parent::getEditorHtml($element);

        return $html;
    }

    /**
     * @param Support_ProjectModel|BaseElementModel $element
     * @param array $params
     * @return bool
     */
    public function saveElement(BaseElementModel $element, $params)
    {
        SupportProjectHelper::populateProjectModel($element, $params);

        return craft()->support_projects->saveProject($element);
    }
}
