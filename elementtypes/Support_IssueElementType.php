<?php

namespace Craft;
use Support\Helper\SupportIssueHelper;

/**
 * Class Support_IssueElementType
 *
 * @package Craft
 */
class Support_IssueElementType extends BaseElementType
{
    /**
     * Returns the element type name.
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Issues');
    }

    /**
     * @inheritDoc
     */
    public function hasStatuses()
    {
        return true;
    }

    /**
     * Returns whether this element type has content.
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function hasTitles()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isLocalized()
    {
        return false;
    }

    /**
     * @param null $source
     *
     * @return array
     */
    public function getAvailableActions($source = null)
    {
        $actions = [];

        if (craft()->userSession->checkPermission('support-manageIssues'))
        {
            $deleteAction = craft()->elements->getAction('Delete');

            $deleteAction->setParams([
                'confirmationMessage' => Craft::t('Are you sure you want to delete the selected issues?'),
                'successMessage' => Craft::t('Issues deleted.'),
            ]);

            $actions[] = $deleteAction;
        }

        return $actions;
    }

    /**
     * Returns this element type's sources.
     *
     * @param string|null $context
     * @return array|false
     */
    public function getSources($context = null)
    {
        $sources = [
            '*' => [
                'label' => Craft::t('All Issues'),
                'defaultSort' => ['dateCreated', 'asc']
            ],
        ];

        $sources[] = ['heading' => Craft::t('Status')];

        $openIssueStatusIds = join(',', craft()->support_issueStatuses->getOpenIssueStatuses());
        $closedIssueStatusIds = join(',', craft()->support_issueStatuses->getClosedIssueStatuses());

        $sources['status:open'] = [
            'status' => 'green',
            'label' => Craft::t('Open issues'),
            'criteria' => [
                'issueStatusId' => $openIssueStatusIds,
                ['and',
                    'elements.enabled = 1', 'elements_i18n.enabled = 1']
            ],
            'defaultSort' => ['dateCreated', 'asc'],
        ];

        $sources['status:closed'] = [
            'status' => 'red',
            'label' => Craft::t('Closed issues'),
            'criteria' => [
                'issueStatusId' => $closedIssueStatusIds,
                ['and',
                    'elements.enabled = 1', 'elements_i18n.enabled = 1']
            ],
            'defaultSort' => ['dateCreated' ,'asc'],
        ];

        $sources[] = ['heading' => Craft::t('Projects')];

        $projects = craft()->support_projects->findAllFavoriteProjects();

        foreach ($projects as $project)
        {
            $sources['project:' . $project->id] = [
                'label' => $project->name,
                'criteria' => ['projectId' => $project->id],
                'defaultSort' => ['dateCreated', 'asc'],
            ];
        }

        return $sources;
    }

    /**
     * Returns the attributes that can be shown/sorted by in table views.
     *
     * @return array
     */
    public function defineAvailableTableAttributes()
    {
        return [
            'title'          => Craft::t('Title'),
            'id'             => Craft::t('Issue nr'),
            'issueStatus'    => Craft::t('Issue Status'),
            'issueType'      => Craft::t('Issue Type'),
            'project'        => ['label' => Craft::t('Project')],
            'dateCreated'    => ['label' => Craft::t('Date created')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultTableAttributes($source = null)
    {
        $attributes = [];

        $attributes[] = 'issueStatus';
        $attributes[] = 'issueType';
        $attributes[] = 'project';
        $attributes[] = 'dateCreated';

        return $attributes;
    }

    /**
     * @inheritDoc
     */
    public function defineSortableAttributes()
    {
        $attributes = [
            'issueStatus' => Craft::t('Issue Status'),
            'issueType' => Craft::t('Issue Type'),
            'project' => Craft::t('Project'),
            'dateCreated' => Craft::t('Date Created'),
        ];

        return $attributes;
    }

    /**
     * @inheritDoc
     */
    public function defineSearchableAttributes()
    {
        return ['title', 'project', 'description', 'solution'];
    }

    /**
     * Returns the table view HTML for a given attribute.
     *
     * @param Support_IssueModel|BaseElementModel $element
     * @param string $attribute
     * @return mixed|string
     */
    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        switch ($attribute)
        {
            case 'issueStatus':
            {
                $issueStatus = $element->getIssueStatus();

                return ($issueStatus ? $issueStatus->name : '');
            }

            case 'issueStatusId':
            {
                $issueStatusId = $element->issueStatusId;

                if ((int) $issueStatusId > 0)
                {
                    return craft()->support_issueStatuses->getIssueStatusById($issueStatusId);
                }
                else
                {
                    return '';
                }
            }

            case 'issueType':
            {
                $issueType = $element->getIssueType();

                return ($issueType ? $issueType->name : '');
            }

            case 'issueTypeId':
            {
                $issueTypeId = $element->issueTypeId;

                if ((int) $issueTypeId > 0)
                {
                    return craft()->support_issueTypes->getIssueTypeById($issueTypeId);
                }
                else
                {
                    return '';
                }
            }

            case 'project':
            {
                $project = $element->getProject();

                return ($project ? $project->name : '');
            }

            case 'projectId':
            {
                $projectId = $element->projectId;

                if ((int) $projectId > 0)
                {
                    return craft()->support_projects->getProjectById($projectId);
                }
                else
                {
                    return '';
                }
            }

            default:
            {
                return parent::getTableAttributeHtml($element, $attribute);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getStatuses()
    {
        $statuses = craft()->support_issueStatuses->getAllIssueStatuses();

        $output = [];

        foreach ($statuses as $status)
        {
            $output[$status->color . ' ' . $status->id] = $status->name;
        }

        return $output;
    }

    /**
     * Defines any custom element criteria attributes for this element type.
     *
     * @return array
     */
    public function defineCriteriaAttributes()
    {
        return [
            'project'     => AttributeType::Mixed,
            'projectId'   => AttributeType::Mixed,
            'issueType'   => AttributeType::Mixed,
            'issueTypeId' => AttributeType::Mixed,
            'issueStatus'   => AttributeType::Mixed,
            'issueStatusId' => AttributeType::Mixed,
            'description' => AttributeType::String,
            'solution'    => AttributeType::String,
            'order'       => [AttributeType::String, 'default' => 'support_issues.dateCreated asc'],
        ];
    }

    /**
     * Modifies an element query targeting elements of this type.
     *
     * @param DbCommand $query
     * @param ElementCriteriaModel|Support_IssueModel $criteria
     * @return false|null|void
     */
    public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    {
        $query
            ->addSelect(
                'support_issues.projectId, 
                support_issues.issueTypeId, 
                support_issues.issueStatusId, 
                support_issues.solution, 
                support_issues.description')
            ->join(
                'support_issues support_issues',
                'support_issues.id = elements.id')
            ->join(
                'support_issuestatuses support_issuestatuses',
                'support_issuestatuses.id = support_issues.issueStatusId')
            ->join(
                'support_issuetypes support_issuetypes',
                'support_issuetypes.id = support_issues.issueTypeId')
            ->join(
                'support_projects support_projects',
                'support_projects.id = support_issues.projectId');


        // Enable related attribute sorting
        if ($criteria->order)
        {
            $order = (string) $criteria->order;

            if (strpos($order, 'project') !== -1)
            {
                $order = str_replace('project', 'support_projects.name', $order);
            }

            if (strpos($order, 'issueStatus') !== -1)
            {
                $order = str_replace('issueStatus', 'support_issuestatuses.name', $order);
            }

            if (strpos($order, 'issueType') !== -1)
            {
                $order = str_replace('issueType', 'support_issuetypes.name', $order);
            }

            $criteria->order = $order;
        }

        if ($criteria->description)
        {
            $query->andWhere(DbHelper::parseParam('support_issues.description', $criteria->description, $query->params));
        }

        if ($criteria->solution)
        {
            $query->andWhere(DbHelper::parseParam('support_issues.solution', $criteria->solution, $query->params));
        }

        if ($criteria->project)
        {
            if ($criteria->project instanceof Support_ProjectModel)
            {
                $criteria->projectId = $criteria->project->id;
                $criteria->project = null;
            }
            else
            {
                $query->andWhere(DbHelper::parseParam('support_projects.id', $criteria->project, $query->params));
            }
        }

        if ($criteria->projectId)
        {
            $query->andWhere(DbHelper::parseParam('support_issues.projectId', $criteria->projectId, $query->params));
        }

        if ($criteria->issueType)
        {
            if ($criteria->issueType instanceof Support_IssueTypeModel)
            {
                $criteria->issueTypeId = $criteria->issueType->id;
                $criteria->issueType = null;
            }
            else
            {
                $query->andWhere(DbHelper::parseParam('support_issuetypes.id', $criteria->issueType, $query->params));
            }
        }

        if ($criteria->issueTypeId)
        {
            $query->andWhere(DbHelper::parseParam('support_issues.issueTypeId', $criteria->issueTypeId, $query->params));
        }

        if ($criteria->issueStatus)
        {
            if ($criteria->issueStatus instanceof Support_IssueStatusModel)
            {
                $criteria->issueStatusId = $criteria->issueStatus->id;
                $criteria->issueStatus = null;
            }
            else
            {
                $query->andWhere(DbHelper::parseParam('support_issuestatuses.id', $criteria->issueStatus, $query->params));
            }
        }

        if ($criteria->issueStatusId)
        {
            $query->andWhere(DbHelper::parseParam('support_issues.issueStatusId', $criteria->issueStatusId, $query->params));
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getElementQueryStatusCondition(DbCommand $query, $status)
    {
        // Status is build with color and status id, separated with a space
        $statusParts = explode(' ', $status);

        if (! isset($statusParts[1]))
        {
            return [];
        }

        return ['and',
            'elements.enabled = 1',
            'elements_i18n.enabled = 1',
            "support_issues.issueStatusId = '" . $statusParts[1] . "'"
        ];
    }

    /**
     * Populates an element model based on query result.
     *
     * @param array $row
     * @return Support_IssueModel|BaseModel|array
     */
    public function populateElementModel($row)
    {
        return Support_IssueModel::populateModel($row);
    }

    /**
     * Returns the HTML for an editor HUD for the given element.
     *
     * @param BaseElementModel|Support_IssueModel $element
     * @return string The HTML for the editor HUD.
     */
    public function getEditorHtml(BaseElementModel $element)
    {
        $templatesService = craft()->templates;
        $html = '';
        $html .= $templatesService->renderMacro('support/issues/_fields', 'statusField', [$element]);
        $html .= $templatesService->renderMacro('support/issues/_fields', 'typeField', [$element]);
        $html .= $templatesService->renderMacro('support/issues/_fields', 'projectField', [$element]);
        $html .= $templatesService->renderMacro('support/issues/_fields', 'titleField', [$element]);
        $html .= parent::getEditorHtml($element);

        return $html;
    }

    /**
     * @param Support_IssueModel|BaseElementModel $element
     * @param array $params
     * @return bool
     */
    public function saveElement(BaseElementModel $element, $params)
    {
        SupportIssueHelper::populateIssueModel($element, $params);

        return craft()->support_issues->saveIssue($element);
    }
}
