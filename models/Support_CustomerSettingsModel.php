<?php

namespace Craft;

/**
 * Class Support_CustomerSettingsModel
 *
 * @property string $name
 * @property int    $id
 * @property string $handle
 * @property int    $fieldLayoutId
 *
 * @property FieldLayoutRecord $fieldLayout
 *
 * @method null setFieldLayout(FieldLayoutModel $fieldLayout)
 * @method FieldLayoutModel getFieldLayout()
 *
 * @package Craft
 */
class Support_CustomerSettingsModel extends BaseModel
{
    /**
     * Use the translated calendar name as the string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return Craft::t($this->handle);
    }

    /**
     * @return string
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/settings/customersettings');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'fieldLayout' => new FieldLayoutBehavior('Support_Customer'),
        ];
    }

    protected function defineAttributes()
    {
        return [
            'id'            => AttributeType::Number,
            'name'          => AttributeType::String,
            'handle'        => AttributeType::String,
            'fieldLayoutId' => AttributeType::Number,
        ];
    }
}
