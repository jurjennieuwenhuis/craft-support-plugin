<?php

namespace Craft;

/**
 * Class Support_IssueSettingsModel
 *
 * @property int    $id
 * @property string $name
 * @property string $handle
 * @property int    $fieldLayoutId
 *
 * @property FieldLayoutRecord $fieldLayout
 *
 * @method null setFieldLayout(FieldLayoutModel $fieldLayout)
 * @method FieldLayoutModel getFieldLayout()
 *
 * @package Craft
 */
class Support_IssueSettingsModel extends BaseModel
{
    /**
     * @return string
     */
    public function __toString()
    {
        return Craft::t($this->handle);
    }

    /**
     * @return string
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/settings/issuesettings');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'fieldLayout' => new FieldLayoutBehavior('Support_Issue'),
        ];
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'id'            => AttributeType::Number,
            'name'          => AttributeType::String,
            'handle'        => AttributeType::String,
            'fieldLayoutId' => AttributeType::Number,
        ];
    }
}
