<?php

namespace Craft;

/**
 * Settings model.
 *
 * @property string $name
 *
 * @package Craft
 */
class Support_SettingsModel extends BaseModel
{
    public function defineAttributes()
    {
        return [
            'name' => [AttributeType::String],
            'assetSourceId' => [AttributeType::Number],
        ];
    }
}
