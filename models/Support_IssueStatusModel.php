<?php

namespace Craft;

/**
 * Class Support_IssueStatusModel
 *
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property string $color
 * @property int $sortOrder
 * @property bool $default
 * @property bool $closed
 *
 * @package craft.plugins.support.models
 */
class Support_IssueStatusModel extends BaseModel
{
    /**
     * @return string
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/settings/issuestatuses/' . $this->id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return string
     */
    public function htmlLabel()
    {
        return sprintf('<span class="supportIssueStatusLabel">%s</span>',
            $this->name
        );
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'id' => AttributeType::Number,
            'name' => [AttributeType::String, 'required' => true],
            'handle' => [AttributeType::Handle, 'required' => true],
            'color' => [AttributeType::String, 'default' => 'green'],
            'sortOrder' => AttributeType::Number,
            'default' => [AttributeType::Bool, 'default' => false, 'required' => true],
            'closed' => [AttributeType::Bool, 'default' => false, 'required' => true],
        ];
    }
}
