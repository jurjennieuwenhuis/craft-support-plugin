<?php

namespace Craft;

/**
 * Class Support_ProjectModel
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $customerId
 * @property bool $favorite
 * @property bool $enabled
 *
 * @property Support_CustomerModel $customer
 * @property Support_IssueModel[] $issues
 *
 * @package Craft
 */
class Support_ProjectModel extends BaseElementModel
{
    const LIVE = 'live';

    protected $elementType = 'Support_Project';

    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Class overloading
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        $getter = 'get' . $name;

        if (method_exists($this, $getter))
        {
            return $this->{$getter}();
        }

        return parent::__get($name);
    }

    /**
     * @return BaseElementModel|Support_CustomerModel|null
     */
    public function getCustomer()
    {
        if ($this->customerId)
        {
            return craft()->support_customers->getCustomerById($this->customerId);
        }

        return null;
    }

    /**
     * @return Support_IssueModel[]|null
     */
    public function getIssues()
    {
        return craft()->support_issues->getIssuesByProject($this);
    }

    public function isFavorite()
    {
        return (bool) $this->favorite;
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function isEditable()
    {
        return craft()->userSession->checkPermission('support-manageProjects');
    }

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/projects/' . $this->id);
    }

    /**
     * Returns the field layout used by this element.
     *
     * @return FieldLayoutModel|null
     */
    public function getFieldLayout()
    {
        return craft()->support_projectSettings->getProjectSettingByHandle('project')->getFieldLayout();
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        $status = parent::getStatus();

        // We'll want the status indicator to show green.
        if ($status === static::ENABLED)
        {
            return static::LIVE;
        }

        return $status;
    }

    /**
     * @inheritDoc
     */
    public function validate($attributes = null, $clearErrors = true)
    {
        return parent::validate($attributes, false);
    }


    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'name' => AttributeType::String,
            'code' => AttributeType::String,
            'favorite'    => AttributeType::Bool,
            'customerId'  => AttributeType::Number,
        ]);
    }
}
