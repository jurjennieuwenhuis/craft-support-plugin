<?php

namespace Craft;

/**
 * Class Support_IssueTypeModel
 *
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property int $sortOrder
 * @property bool $default
 *
 * @package craft.plugins.support.models
 */
class Support_IssueTypeModel extends BaseModel
{
    /**
     * @return string
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/settings/issuetypes/' . $this->id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return string
     */
    public function htmlLabel()
    {
        return sprintf('<span class="supportIssueTypeLabel">%s</span>',
            $this->name
        );
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'id' => AttributeType::Number,
            'name' => [AttributeType::String, 'required' => true],
            'handle' => [AttributeType::Handle, 'required' => true],
            'sortOrder' => AttributeType::Number,
            'default' => [AttributeType::Bool, 'default' => false, 'required' => true],
        ];
    }
}
