<?php

namespace Craft;

/**
 * Class Support_IssueModel
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property bool $enabled
 *
 * @property Support_IssueModel[] $issues
 * @property Support_ProjectModel[] $projects
 *
 * @package Craft
 */
class Support_CustomerModel extends BaseElementModel
{
    protected $elementType = 'Support_Customer';

    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Class overloading
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        $getter = 'get' . $name;

        if (method_exists($this, $getter))
        {
            return $this->{$getter}();
        }

        return parent::__get($name);
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function isEditable()
    {
        return craft()->userSession->checkPermission('support-manageCustomers');
    }

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/customers/' . $this->id);
    }

    /**
     * Returns the field layout used by this element.
     *
     * @return FieldLayoutModel|null
     */
    public function getFieldLayout()
    {
        return craft()->support_customerSettings->getCustomerSettingByHandle('customer')->getFieldLayout();
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        parent::getStatus();
    }

    /**
     * @inheritDoc
     */
    public function validate($attributes = null, $clearErrors = true)
    {
        return parent::validate($attributes, false);
    }


    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'name' => AttributeType::String,
            'code' => AttributeType::Handle,
        ]);
    }
}
