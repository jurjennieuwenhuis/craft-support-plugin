<?php

namespace Craft;

/**
 * Class Support_IssueModel
 *
 * @property int $id
 * @property int $projectId
 * @property int $issueTypeId
 * @property int $issueStatusId
 * @property string $title
 * @property string $description
 * @property string $solution
 * @property array $assetIds
 *
 * @property Support_CustomerModel $customer
 * @property Support_ProjectModel $project
 * @property Support_IssueTypeModel $issueType
 * @property Support_IssueStatusModel $issueStatus
 *
 * @package Craft
 */
class Support_IssueModel extends BaseElementModel
{

    protected $elementType = 'Support_Issue';

    /**
     * Class overloading
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);

        if (method_exists($this, $getter))
        {
            return $this->{$getter}();
        }

        return parent::__get($name);
    }

    /**
     * Returns whether the current user can edit the element.
     *
     * @return bool
     */
    public function isEditable()
    {
        return craft()->userSession->checkPermission('support-manageIssues');
    }

    /**
     * Returns the element's CP edit URL.
     *
     * @return string|false
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('support/issues/' . $this->id);
    }

    /**
     * Returns the field layout used by this element.
     *
     * @return FieldLayoutModel|null
     */
    public function getFieldLayout()
    {
        return craft()->support_issueSettings->getIssueSettingByHandle('issue')->getFieldLayout();
    }

    /**
     * Returns the customer
     *
     * @return BaseElementModel|Support_CustomerModel|null
     */
    public function getCustomer()
    {
        if ($this->projectId)
        {
            return $this->getProject()->getCustomer();
        }

        return null;
    }

    /**
     * Returns the project.
     *
     * @return Support_ProjectModel|null
     */
    public function getProject()
    {
        if ($this->projectId)
        {
            return craft()->support_projects->getProjectById($this->projectId);
        }

        return null;
    }

    /**
     * @return Support_IssueTypeModel|BaseModel|null
     */
    public function getIssueType()
    {
        return craft()->support_issueTypes->getIssueTypeById($this->issueTypeId);
    }

    /**
     * @return Support_IssueStatusModel|BaseModel|null
     */
    public function getIssueStatus()
    {
        return craft()->support_issueStatuses->getIssueStatusById($this->issueStatusId);
    }

    /**
     * @deprecated Use getAssets instead
     *
     * @return ElementCriteriaModel
     */
    public function getFiles()
    {
        return $this->getAssets();
    }

    /**
     * @return ElementCriteriaModel|null
     */
    public function getAssets()
    {
        if ($this->id)
        {
            return craft()->support_issues->getFiles($this->id);
        }

        return null;
    }

    public function getStatus()
    {
        if ($this->issueStatusId)
        {
            return craft()->support_issueStatuses->getIssueStatusById($this->issueStatusId)->color;
        }

        return BaseElementModel::ENABLED;
    }

    public function validate($attributes = null, $clearErrors = true)
    {
//        if (empty($this->issueStatus))
//        {
//            $this->addError('issueStatus', Craft::t('Issue status is required.'));
//        }

        if (empty($this->projectId))
        {
            $this->addError('projectId', Craft::t('Please select a project'));
        }

        if (empty($this->issueStatusId))
        {
            $this->addError('issueStatusId', Craft::t('Please select a status'));
        }

        if (empty($this->issueTypeId))
        {
            $this->addError('issueTypeId', Craft::t('Please select a type'));
        }

        return parent::validate($attributes, false);
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), [
            'projectId'  => AttributeType::Number,
            'issueStatusId' => AttributeType::Number,
            'description' => AttributeType::String,
            'solution' => AttributeType::String,
            'assetIds'    => AttributeType::Mixed,
            'issueTypeId' => AttributeType::Number,
        ]);
    }

}
