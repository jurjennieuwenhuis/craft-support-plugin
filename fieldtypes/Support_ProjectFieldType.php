<?php

namespace Craft;


class Support_ProjectFieldType extends BaseElementFieldType
{
    /**
     * @inheritDoc
     */
    protected $elementType = 'Support_Project';

    /**
     * @inheritDoc
     *
     * @return string|null
     */
    public function getName()
    {
        return Craft::t('Support Projects');
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    protected function getAddButtonLabel()
    {
        return Craft::t('Add a project');
    }
}
