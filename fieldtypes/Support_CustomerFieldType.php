<?php

namespace Craft;


class Support_CustomerFieldType extends BaseElementFieldType
{
    /**
     * @inheritDoc
     */
    protected $elementType = 'Support_Customer';

    /**
     * @inheritDoc
     *
     * @return string|null
     */
    public function getName()
    {
        return Craft::t('Support Customers');
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    protected function getAddButtonLabel()
    {
        return Craft::t('Add a customer');
    }
}
