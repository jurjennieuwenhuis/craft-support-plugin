<?php

namespace Craft;


class Support_IssueFieldType extends BaseElementFieldType
{
    /**
     * @inheritDoc
     */
    protected $elementType = 'Support_Issue';

    /**
     * @inheritDoc
     *
     * @return string|null
     */
    public function getName()
    {
        return Craft::t('Support Issues');
    }

    /**
     * @inheritDoc
     *
     * @return string
     */
    protected function getAddButtonLabel()
    {
        return Craft::t('Add an issue');
    }
}
