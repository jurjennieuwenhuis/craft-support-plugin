<?php

namespace Craft;

require 'vendor/autoload.php';

/**
 * Support plugin class
 */
class SupportPlugin extends BasePlugin
{
    private $doSeed = true;

    private static $_elementThumbSizes = [30, 60, 100, 200];

    public function init()
    {
        // If this is a CP request, register the events.prepCpTemplate hook
        if (craft()->request->isCpRequest())
        {
            craft()->templates->hook('support.prepCpTemplate', [$this, 'prepCpTemplate']);


            // Register the cp.elements.element hook
            craft()->templates->hook('support.cp.elements.element', [$this, 'getCpElementHtml']);


            // Init event handlers
            craft()->on('support_projects.saveSupportProject', [craft()->support_issues, 'projectStatusChangeHandler']);
        }
    }

    public function testCallback($event)
    {
        $foo = 'bar';
    }

    public function getName()
    {
        return 'Support';
    }

    public function getVersion()
    {
        return '1.0';
    }

    public function getSchemaVersion()
    {
        return '1.0';
    }

    public function getDeveloper()
    {
        return 'Juni';
    }

    public function getDeveloperUrl()
    {
        return 'http://www.kasanova.nl';
    }

    public function getPluginUrl()
    {
        return 'https://bitbucket.org/jurjennieuwenhuis/craft-support-plugin';
    }

    public function hasCpSection()
    {
        return true;
    }

    public function onBeforeInstall()
    {
        if (! defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50400)
        {
            Craft::log('Craft Commerce requires PHP 5.4+ in order to run.', LogLevel::Error);

            return false;
        }

        return true;
    }

    /**
     * After install, run seeders and optional test data.
     *
     */
    public function onAfterInstall()
    {
        if ($this->doSeed) {
            craft()->support_seed->afterInstall();
        }
    }

    public function onBeforeUninstall()
    {
        // Delete the issue element index settings
        $productsElementSettings = ElementIndexSettingsRecord::model()->findByAttributes(['type' => 'Support_Issue']);
        if ($productsElementSettings)
        {
            $productsElementSettings->delete();
        }

        // Delete the customer element index settings
        $customerElementSettings = ElementIndexSettingsRecord::model()->findByAttributes(['type' => 'Support_Customer']);
        if ($customerElementSettings)
        {
            $customerElementSettings->delete();
        }

        // Delete the project element index settings
        $projectElementSettings = ElementIndexSettingsRecord::model()->findByAttributes(['type' => 'Support_Project']);
        if ($projectElementSettings)
        {
            $projectElementSettings->delete();
        }
    }

    public function registerCpRoutes()
    {
        return require(__DIR__ . '/etc/routes.php');
    }

    /**
     * Adds custom link options to Rich Text fields.
     *
     * @return array
     */
    public function addRichTextLinkOptions()
    {
        $linkOptions = [];

        $linkOptions[] = [
            'optionTitle' => Craft::t('Link to an issue'),
            'elementType' => 'Support_Issue',
        ];

        $linkOptions[] = [
            'optionTitle' => Craft::t('Link to a customer'),
            'elementType' => 'Support_Customer',
        ];

        $linkOptions[] = [
            'optionTitle' => Craft::t('Link to a project'),
            'elementType' => 'Support_Project',
        ];

        return $linkOptions;
    }

    /**
     * @inheritDoc
     */
    public function getSettingsUrl()
    {
        return 'support/settings/general';
    }

    public function prepCpTemplate(&$context)
    {
        $context['subnav'] = [];
        $context['subnav']['issues']    = ['label' => Craft::t('Issues'), 'url' => 'support/issues'];
        $context['subnav']['projects']  = ['label' => Craft::t('Projects'), 'url' => 'support/projects'];
        $context['subnav']['customers'] = ['label' => Craft::t('Customers'), 'url' => 'support/customers'];

        if (craft()->userSession->isAdmin())
        {
            $context['subnav']['settings']    = ['label' => Craft::t('Settings'), 'url' => 'support/settings'];
        }
    }


    /**
     * Hook into craft to register user permissions.
     *
     * @return array
     */
    public function registerUserPermissions()
    {
        return [
            'support-manageIssues' => ['label' => Craft::t('Manage issues')],
            'support-manageProjects' => ['label' => Craft::t('Manage projects')],
            'support-manageCustomers' => ['label' => Craft::t('Manage customers')],
        ];
    }


    protected function defineSettings()
    {
        $settingsModel = new Support_SettingsModel();
        return $settingsModel;
    }

    /**
     * Returns the HTML for an element in the CP.
     *
     * @param array &$context
     *
     * @return string|null
     */
    public function getCpElementHtml(&$context)
    {
        if (!isset($context['id']) || $context['id'] != 'files')
        {
            return null;
        }

        if (!isset($context['element']))
        {
            return null;
        }

        if (!isset($context['context']))
        {
            $context['context'] = 'index';
        }

        if (isset($context['elementType']))
        {
            $elementType = $context['elementType'];
        }
        else
        {
            $elementType = craft()->elements->getElementType($context['element']->getElementType());
        }

        // How big is the element going to be?
        if (isset($context['size']) && ($context['size'] == 'small' || $context['size'] == 'large'))
        {
            $elementSize = $context['size'];
        }
        else if (isset($context['viewMode']) && $context['viewMode'] == 'thumbs')
        {
            $elementSize = 'large';
        }
        else
        {
            $elementSize = 'small';
        }

        // Create the thumb/icon image, if there is one
        // ---------------------------------------------------------------------

        $thumbUrl = $context['element']->getThumbUrl(self::$_elementThumbSizes[0]);

        if ($thumbUrl)
        {
            $srcsets = [];

            foreach (self::$_elementThumbSizes as $i => $size)
            {
                if ($i == 0)
                {
                    $srcset = $thumbUrl;
                }
                else
                {
                    $srcset = $context['element']->getThumbUrl($size);
                }

                $srcsets[] = $srcset.' '.$size.'w';
            }

            $imgHtml = '<div class="elementthumb">'.
                '<img '.
                'sizes="'.($elementSize == 'small' ? self::$_elementThumbSizes[0] : self::$_elementThumbSizes[2]).'px" '.
                'srcset="'.implode(', ', $srcsets).'" '.
                'alt="">'.
                '</div> ';
        }
        else
        {
            $imgHtml = '';
        }

        $html = '<div class="element '.$elementSize;

        if ($context['context'] == 'field')
        {
            $html .= ' removable';
        }

        if ($elementType->hasStatuses())
        {
            $html .= ' hasstatus';
        }

        if ($thumbUrl)
        {
            $html .= ' hasthumb';
        }

        $label = HtmlHelper::encode($context['element']);

        $html .= '" data-id="'.$context['element']->id.'" data-locale="'.$context['element']->locale.'" data-status="'.$context['element']->getStatus().'" data-label="'.$label.'" data-url="'.$context['element']->getUrl().'"';

        if ($context['element']->level)
        {
            $html .= ' data-level="'.$context['element']->level.'"';
        }

        $isEditable = ElementHelper::isElementEditable($context['element']);

        if ($isEditable)
        {
            $html .= ' data-editable';
        }

        $html .= '>';

        if ($context['context'] == 'field' && isset($context['name']))
        {
            $html .= '<input type="hidden" name="'.$context['name'].'[]" value="'.$context['element']->id.'">';
            $html .= '<a class="delete icon" title="'.Craft::t('Remove').'"></a> ';
        }

        if ($elementType->hasStatuses())
        {
            $html .= '<span class="status '.$context['element']->getStatus().'"></span>';
        }

        $html .= $imgHtml;
        $html .= '<div class="label">';

        $html .= '<span class="title">';

        if ($context['context'] == 'field' && ($cpEditUrl = $context['element']->getUrl()))
        {
            $cpEditUrl = HtmlHelper::encode($cpEditUrl);
            $html .= "<a href=\"{$cpEditUrl}\" target=\"_blank\">Test {$label}</a>";
        }
        else
        {
            $html .= $label;
        }

        $html .= '</span></div></div>';

        return $html;
    }
}
