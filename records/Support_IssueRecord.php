<?php

namespace Craft;

/**
 * Class Support_IssueRecord
 *
 * @property int $id
 * @property int $projectId
 * @property int $issueTypeId
 * @property int $issueStatusId
 * @property string $description
 * @property string $solution
 *
 * @property Support_IssueTypeRecord $issueType
 * @property Support_IssueStatusRecord $issueStatus
 *
 * @package Craft
 */
class Support_IssueRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'support_issues';
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [
                static::BELONGS_TO,
                'ElementRecord',
                'id',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'project' => [
                static::BELONGS_TO,
                'Support_ProjectRecord',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'issueType' => [
                static::BELONGS_TO,
                'Support_IssueTypeRecord',
                'onDelete' => static::RESTRICT,
                'onUpdate' => self::CASCADE,
                'required' => true,
            ],
            'issueStatus' => [
                static::BELONGS_TO,
                'Support_IssueStatusRecord',
                'onDelete' => static::RESTRICT,
                'onUpdate' => self::CASCADE,
                'required' => true,
            ]
        ];
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'description' => [AttributeType::String, 'column' => ColumnType::Text, 'required' => true],
            'solution'    => [AttributeType::String, 'column' => ColumnType::Text, 'required' => false],
        ];
    }
}
