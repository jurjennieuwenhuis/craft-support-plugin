<?php

namespace Craft;

/**
 * Class Support_IssueFileRecord
 *
 * @property string $name
 * @property string $handle
 * @property int    $fieldLayoutId
 *
 * @package Craft
 */
class Support_IssueFileRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'support_issue_files';
    }

    /**
     * @return array
     */
    public function defineIndexes()
    {
        return [
            ['columns' => ['issueId', 'assetId'], 'unique' => true],
        ];
    }

    public function defineRelations()
    {
        return [
            'issue'  => [
                static::BELONGS_TO,
                'Support_IssueRecord',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'asset'  => [
                static::BELONGS_TO,
                'AssetFileRecord',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
        ];
    }

    protected function defineAttributes()
    {
        return [
            'issueId'  => [AttributeType::Number, 'required' => true],
            'assetId'  => [AttributeType::Number, 'required' => true],
        ];
    }
}
