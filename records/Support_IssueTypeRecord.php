<?php

namespace Craft;

/**
 * Class Support_IssueTypeRecord
 *
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property int $sortOrder
 * @property bool $default
 *
 * @package craft.plugins.support.records
 */
class Support_IssueTypeRecord extends BaseRecord
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'support_issuetypes';
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'name' => [AttributeType::String, 'required' => true],
            'handle' => [AttributeType::Handle, 'required' => true],
            'sortOrder' => AttributeType::SortOrder,
            'default' => [AttributeType::Bool, 'default' => 0, 'required' => true],
        ];
    }
}
