<?php

namespace Craft;

/**
 * Class Support_CustomerRecord
 *
 * @property int $id
 * @property string $name
 * @property string $code
 *
 * @package Craft
 */
class Support_CustomerRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'support_customers';
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [
                static::BELONGS_TO,
                'ElementRecord',
                'id',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'issues' => [
                static::HAS_MANY,
                'Support_IssueRecord',
                'customerId',
            ]
        ];
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'name' => [AttributeType::String, 'required' => true],
            'code' => [AttributeType::Handle, 'required' => true, 'unique' => true],
        ];
    }

}
