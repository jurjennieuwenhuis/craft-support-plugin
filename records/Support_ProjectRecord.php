<?php

namespace Craft;

/**
 * Class Support_ProjectRecord
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property bool $favorite
 * @property int $customerId
 * @property bool $enabled
 *
 * @package Craft
 */
class Support_ProjectRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'support_projects';
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [
                static::BELONGS_TO,
                'ElementRecord',
                'id',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'issues' => [
                static::HAS_MANY,
                'Support_IssueRecord',
                'projectId',
            ],
            'customer' => [
                static::BELONGS_TO,
                'Support_CustomerRecord',
                'required' => true,
                'onDelete' => static::CASCADE,
            ]
        ];
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'name'     => [AttributeType::String, 'required' => true],
            'code'     => [AttributeType::String, 'required' => true, 'unique' => true],
            'favorite' => [AttributeType::Bool, 'default' => false],
        ];
    }

}
