<?php

namespace Craft;

/**
 * Class Support_ProjectSettingsRecord
 *
 * @property int    $id
 * @property string $name
 * @property string $handle
 * @property int    $fieldLayoutId
 *
 * @property FieldLayoutRecord $fieldLayout
 *
 * @package Craft
 */
class Support_ProjectSettingsRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'support_projectsettings';
    }

    public function defineIndexes()
    {
        return [
            ['columns' => ['handle'], 'unique' => true],
        ];
    }

    public function defineRelations()
    {
        return [
            'fieldLayout' => [
                static::BELONGS_TO,
                'FieldLayoutRecord',
                'onDelete' => static::SET_NULL
            ],
        ];
    }

    protected function defineAttributes()
    {
        return [
            'name' => [AttributeType::Name, 'required' => true],
            'handle' => [AttributeType::Handle, 'required' => true],
        ];
    }
}
