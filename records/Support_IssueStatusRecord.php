<?php

namespace Craft;

/**
 * Class Support_IssueStatusRecord
 *
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property string $color
 * @property int $sortOrder
 * @property bool $default
 * @property bool $closed
 *
 * @package craft.plugins.support.records
 */
class Support_IssueStatusRecord extends BaseRecord
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'support_issuestatuses';
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'name' => [AttributeType::String, 'required' => true],
            'handle' => [AttributeType::Handle, 'required' => true],
            'color' => [AttributeType::Enum, 'values' => [
                'green',
                'orange',
                'red',
                'blue',
                'yellow',
                'pink',
                'purple',
                'turquoise',
                'light',
                'grey',
                'black',
            ], 'required' => true, 'default' => 'green'],
            'sortOrder' => AttributeType::Number,
            'default' => [AttributeType::Bool, 'default' => 0, 'required' => true],
            'closed' => [AttributeType::Bool, 'default' => 0, 'required' => true],
        ];
    }
}
