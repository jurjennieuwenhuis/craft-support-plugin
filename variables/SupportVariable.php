<?php

namespace Craft;

class SupportVariable
{
    /** @var  ElementsService */
    private $elements;

    public function __construct()
    {
        $this->elements = craft()->elements;
    }

    public function issues($criteria = null)
    {
        return $this->elements->getCriteria('Support_Issue', $criteria);
    }

    public function customers($criteria = null)
    {
        return $this->elements->getCriteria('Support_Customer', $criteria);
    }

    public function projects($criteria = null)
    {
        return $this->elements->getCriteria('Support_Project', $criteria);
    }

    public function settings()
    {
        return craft()->support_settings->getSettings();
    }

    public function getIssueStatuses()
    {
        return craft()->support_issueStatuses->getAllIssueStatuses();
    }

    public function getIssueStatusOptionList()
    {
        return [0 => Craft::t('Select status')] + craft()->support_issueStatuses->getOptionList();
    }

    public function getIssueTypes()
    {
        return [0 => Craft::t('Select type')] + craft()->support_issueTypes->getOptionList();
    }

    public function getProjects()
    {
        return [0 => Craft::t('Select project')] + craft()->support_projects->getOptionList();
    }

    public function getCustomers()
    {
        return [0 => Craft::t('Select customer')] + craft()->support_customers->getOptionList();
    }
}
