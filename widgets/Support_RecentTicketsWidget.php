<?php

namespace Craft;


class Support_RecentTicketsWidget extends BaseWidget
{
    /**
     * @var bool
     */
    public $multipleInstances = true;

    public function getName()
    {
        return Craft::t('Recent Tickets');
    }

    public function getSettingsHtml()
    {
        return craft()->templates->render('support/widgets/RecentTickets/settings', [
            'settings' => $this->getSettings(),
            'issueStatuses' => [0 => 'Select an issue status'] + craft()->support_issueStatuses->getOptionList(),
        ]);
    }

    public function getTitle()
    {
        $title = $this->getSettings()->title;

        if (! $title)
        {
            $title = Craft::t('Recent issues');
        }

        return $title;
    }

    public function getIconPath()
    {
        return craft()->path->getResourcesPath() . 'images/widgets/recent-tickets.svg';
    }

    public function getBodyHtml()
    {
        $params = [];

        $params['elementType'] = 'Support_Issue';

        $js = 'new Craft.RecentEntriesWidget(' . $this->model->id . ', ' . JsonHelper::encode($params) . ');';

        //craft()->templates->includeJsResource('support/js/RecentEntriesWidget.js');
        //craft()->templates->includeJs($js);

        $issues = $this->getIssues();

        return craft()->templates->render('support/widgets/RecentTickets/body', [
            'issues' => $issues,
        ]);
    }

    protected function defineSettings()
    {
        return [
            'title'  => [AttributeType::String],
            'status'  => [AttributeType::Number, 'default' => 0],
            'limit'   => [AttributeType::Number, 'default' => 10],
        ];
    }

    private function getIssues()
    {
        $limit = $this->getSettings()->limit;
        $statusId = $this->getSettings()->status;

        if (! $limit)
        {
            $limit = 10;
        }

        if (! $statusId)
        {
            $statusId = craft()->support_issueStatuses->getDefaultIssueStatus()->id;
        }

        $criteria = craft()->elements->getCriteria('Support_Issue');
        $criteria->status = null;
        $criteria->editable = true;
        $criteria->limit = $limit;
        $criteria->issueStatusId = $statusId;
        $criteria->order = 'support_issues.dateCreated desc';

        return $criteria->find();
    }
}
