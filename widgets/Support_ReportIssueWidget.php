<?php

namespace Craft;

/**
 * Class Support_ReportIssueWidget
 *
 * @package   craft.plugins.support.widgets
 */
class Support_ReportIssueWidget extends BaseWidget
{
	// Properties
	// =========================================================================

	/**
	 * @var bool
	 */
	public $multipleInstances = false;

	// Public Methods
	// =========================================================================

	/**
	 * @inheritDoc IComponentType::getName()
	 *
	 * @return string
	 */
	public function getName()
	{
		return Craft::t('Report Issue');
	}

	/**
	 * @inheritDoc ISavableComponentType::getSettingsHtml()
	 *
	 * @return string
	 */
	public function getSettingsHtml()
	{
		return craft()->templates->render('support/widgets/ReportIssue/settings', [
			'settings' => $this->getSettings(),
            'issueSettings' => craft()->support_issueSettings->getIssueSettingByHandle('issue'),
        ]);
	}

	/**
	 * Preps the settings before they're saved to the database.
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function prepSettings($settings)
	{
		return $settings;
	}

	/**
	 * @inheritDoc IWidget::getIconPath()
	 *
	 * @return string
	 */
	public function getIconPath()
	{
		return craft()->path->getResourcesPath().'images/widgets/quick-post.svg';
	}

	/**
	 * @inheritDoc IWidget::getTitle()
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getName();
	}

	/**
	 * @inheritDoc IWidget::getBodyHtml()
	 *
	 * @return string|false
	 */
	public function getBodyHtml()
	{
		craft()->templates->includeJsResource('support/js/ReportIssueWidget.js');

		craft()->templates->startJsBuffer();

        $issue = Support_IssueModel::populateModel([
            'issueStatusId' => craft()->support_issueStatuses->getDefaultIssueStatus()->id,
            'issueTypeId' => craft()->support_issueTypes->getDefaultIssueType()->id,
        ]);

		$html = craft()->templates->render('support/widgets/ReportIssue/body', [
		    'issue' => $issue,
			'settings'  => $this->getSettings(),
            'issueSettings' => craft()->support_issueSettings->getIssueSettingByHandle('issue'),
        ]);

        $params = [];

		$fieldJs = craft()->templates->clearJsBuffer(false);

		craft()->templates->includeJs('new Craft.ReportIssueWidget(' .
			$this->model->id.', ' .
			JsonHelper::encode($params).', ' .
			"function() {\n".$fieldJs .
		"\n});");

		return $html;
	}

	// Protected Methods
	// =========================================================================

	/**
	 * @inheritDoc BaseSavableComponentType::defineSettings()
	 *
	 * @return array
	 */
	protected function defineSettings()
	{
		return [
			'fields'    => AttributeType::Mixed,
        ];
	}

	// Private Methods
	// =========================================================================
}
