<?php

namespace Support;

use \Support\Collection\BaseEnum;
use \Craft\Craft;

abstract class IssueStatus extends BaseEnum
{
    const STATUS_NEW       = 'new';
    const STATUS_OPEN      = 'open';
    const STATUS_RESOLVED  = 'resolved';
    const STATUS_DUPLICATE = 'duplicate';
    const STATUS_WONTFIX   = 'wontfix';
    const STATUS_CLOSED    = 'closed';
    const STATUS_ONHOLD    = 'onhold';
    const STATUS_INVALID   = 'invalid';


    /**
     * Returns an array of status label indexed by the status handle.
     *
     * @return array
     */
    public static function getLabels()
    {
        return [
            self::STATUS_NEW        => Craft::t('New'),
            self::STATUS_OPEN       => Craft::t('Open'),
            self::STATUS_RESOLVED   => Craft::t('Resolved'),
            self::STATUS_DUPLICATE  => Craft::t('Duplicate'),
            self::STATUS_WONTFIX    => Craft::t('Wont fix'),
            self::STATUS_CLOSED     => Craft::t('Closed'),
            self::STATUS_ONHOLD     => Craft::t('On Hold'),
            self::STATUS_INVALID    => Craft::t('Invalid'),
        ];
    }


    /**
     * Returns the label of the status based on the status handle.
     *
     * @param string $statusHandle
     * @return string|null
     */
    public static function getLabel($statusHandle)
    {
        $labels = self::getLabels();

        if (isset($labels[$statusHandle]))
        {
            return $labels[$statusHandle];
        }

        return null;
    }

    public static function getOpenStatuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_OPEN,
            self::STATUS_RESOLVED,
        ];
    }

    public static function getClosedStatuses()
    {
        return [
            self::STATUS_DUPLICATE,
            self::STATUS_WONTFIX,
            self::STATUS_CLOSED,
            self::STATUS_ONHOLD,
            self::STATUS_INVALID,
        ];
    }
}
