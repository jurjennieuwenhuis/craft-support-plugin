<?php

namespace Support\Collection;

/**
 * Class EnumIterator
 *
 * EnumIterator allows iteration through an Enum's constants.
 * It is instantiated by the abstract class Enum and should not
 * be instantiated otherwise.
 *
 * @package Support\Collection
 */
final class EnumIterator implements \Iterator
{
    /**
     * @var array
     */
    private $members = [];


    /**
     * @var int
     */
    private $key = 0;


    /**
     * @param string $className Class name of the enum to iterate through
     */
    public function __construct($className)
    {
        $reflectionClass = new \ReflectionClass($className);
        $members = $reflectionClass->getConstants();

        foreach ($members as $value)
        {
            $this->members[] = $value;
        }
    }


    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->key = 0;
    }


    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->members[$this->key];
    }


    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->key;
    }


    /**
     * @inheritDoc
     */
    public function next()
    {
        return $this->key++;
    }


    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->members[$this->key]);
    }

    /**
     * Returns all values
     *
     * @return array
     */
    public function toArray()
    {
        return $this->members;
    }

}
