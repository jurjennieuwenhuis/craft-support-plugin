<?php

namespace Support\Collection;

/**
 * Class BaseEnum
 *
 * There is no Enum in PHP so we try to emulate Enum with this class as much as possible.
 * Any class subclassing EnumBase should also be abstract cause Enums should not be instantiated.
 * Ideally EnumBase would be iterable (extending Iterator) but it's abstract so unfortunately that's not possible. So we
 * implemented an iterator and gave EnumBase the iterator-logic but with static methods instead of not-static methods.
 *
 * Usage:
 *
 * City::rewind();
 *
 * while(City::valid()) {
 *     $city = City::current();
 *     City::next();
 *     echo $city . '<br>';
 * }
 *
 * @package Support\Collection
 */
abstract class BaseEnum
{
    /**
     * Store the iterators for the called Enums here. For internal use only
     * @var EnumIterator[]
     */
    private static $iterators = array();

    /**
     * @return EnumIterator
     */
    public static function getIterator()
    {
        return static::loadIterator();
    }

    /**
     * Lazy initialization of the iterator for the called Enum.
     * @return EnumIterator
     */
    private final static function loadIterator()
    {
        $className = get_called_class();

        if (! isset(self::$iterators[$className]))
        {
            self::$iterators[$className] = new EnumIterator($className);
        }

        return self::$iterators[$className];
    }


    /**
     * Rewind the iterator to the starting position.
     */
    public static function rewind()
    {
        self::loadIterator()->rewind();
    }


    /**
     * Get the current constant value
     * @return string
     */
    public static function current()
    {
        self::loadIterator()->current();
    }


    /**
     * Get the index for the current iteration
     *
     * @return int
     */
    public static function key()
    {
        return self::loadIterator()->key();
    }


    /**
     * Move cursor up.
     */
    public static function next()
    {
        self::loadIterator()->next();
    }


    /**
     * Check if we can move the cursor up.
     *
     * @return bool
     */
    public static function valid()
    {
        return self::loadIterator()->valid();
    }


    /**
     * Returns the value of the constant at the supplied index.
     *
     * @param int $index
     * @return mixed
     * @throws IndexOutOfRangeException
     */
    public static function valueOfIndex($index)
    {
        $values = self::toArray(get_called_class());

        if ($index >= count($values))
        {
            throw new IndexOutOfRangeException(__CLASS__ . " : IndexOutOfRangeException in '" . __METHOD__ . "'");
        }

        return $values[$index];
    }

    /**
     * Returns the position of the list of $needle. Returns -1 if not found.
     *
     * @param mixed $needle
     * @return int
     */
    public static function indexOf($needle)
    {
        $values = self::toArray(get_called_class());
        $index = 0;

        foreach ($values as $value)
        {
            if ($value == $needle)
            {
                return $index;
            }

            $index++;
        }

        return -1;
    }


    /**
     * Converts the constants to an indexed array where the constant names are the keys.
     *
     * @return array
     */
    public static function toDictionary()
    {
        $reflectionClass = new \ReflectionClass(get_called_class());

        return $reflectionClass->getConstants();
    }


    /**
     * @return array
     */
    public static function toArray()
    {
        return self::loadIterator()->toArray();
    }
}
