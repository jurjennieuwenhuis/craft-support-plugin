<?php

namespace Support;

use Support\Helper\SupportDbHelper;

class DisableIssuesEventHandler
{
    public static function handle($project, $oldProject)
    {
        $isNewProject = ! $project->id;

        if (! $isNewProject)
        {

            if ($oldProject->enabled === $project->enabled)
            {
                return;
            }

            $issues = \Craft\craft()->support_issues->getIssuesByProject($project);

            SupportDbHelper::beginStackedTransaction();

            try {

                foreach ($issues as $issue)
                {
                    if ($issue->enabled != $project->enabled)
                    {
                        $issue->enabled = $project->enabled;
                        \Craft\craft()->elements->saveElement($issue);
                    }
                }

                SupportDbHelper::commitStackedTransaction();

            }
            catch (\Exception $e)
            {
                SupportDbHelper::rollbackStackedTransaction();

                throw $e;
            }
        }
    }
}
