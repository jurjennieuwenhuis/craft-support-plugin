<?php

namespace Support\Helper;

class SupportDbHelper
{
    private static $transactionStackSize = 0;

    /** @var \CDbTransaction */
    private static $transaction = null;

    /**
     * This method, and the next two special functions which allow making
     * nested transactions with a delayed commit.
     */
    public static function beginStackedTransaction()
    {
        if (self::$transactionStackSize == 0)
        {
            if (null === \Craft\craft()->db->getCurrentTransaction())
            {
                self::$transaction = \Craft\craft()->db->beginTransaction();
            }
            else
            {
                // If we are at zero but 3rd party has a current transaction in play
                self::$transaction = \Craft\craft()->db->getCurrentTransaction();

                // by setting to 1, we will never commit, but whoever started it should.
                self::$transactionStackSize = 1;
            }
        }

        ++self::$transactionStackSize;
    }

    public static function commitStackedTransaction()
    {
        // Decrement only when positive
        self::$transactionStackSize && --self::$transactionStackSize;

        if (self::$transactionStackSize == 0)
        {
            self::$transaction->commit();
        }
    }

    public static function rollbackStackedTransaction()
    {
        // Decrement only when positive
        self::$transactionStackSize && --self::$transactionStackSize;

        if (self::$transactionStackSize == 0)
        {
            self::$transaction->rollback();
        }
    }
}
