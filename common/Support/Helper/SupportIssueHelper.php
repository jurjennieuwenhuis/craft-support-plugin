<?php

namespace Support\Helper;

use Craft\Support_IssueModel;

class SupportIssueHelper
{
    public static function getParam($data, $name, $default = null)
    {
        if (isset($data[$name]) || array_key_exists($name, $data))
        {
            return $data[$name];
        }

        return $default;
    }

    public static function populateIssueModel(Support_IssueModel $issue, $data)
    {
        if (isset($data['projectId']))
        {
            $projectId = $data['projectId'];
            $issue->projectId  = (is_array($projectId) && count($projectId) > 0) ? $projectId[0] : $projectId;
        }

        if (isset($data['enabled']))
        {
            $issue->enabled = $data['enabled'];
        }

        if (isset($data['issueTypeId']))
        {
            $issue->issueTypeId = $data['issueTypeId'];
        }

        if (isset($data['issueStatusId']))
        {
            $issue->issueStatusId = $data['issueStatusId'];
        }

        if (isset($data['description']))
        {
            $issue->description = $data['description'];
        }

        if (isset($data['solution']))
        {
            $issue->solution = $data['solution'];
        }
    }
}
