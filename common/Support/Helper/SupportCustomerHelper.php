<?php

namespace Support\Helper;

use Craft\Support_CustomerModel;

class SupportCustomerHelper
{
    public static function populateCustomerModel(Support_CustomerModel $customer, $data)
    {
        if (isset($data['enabled']))
        {
            $customer->enabled = $data['enabled'];
        }

        if (isset($data['name']))
        {
            $customer->name = $data['name'];
        }

        if (isset($data['code']))
        {
            $customer->code = $data['code'];
        }
    }
}
