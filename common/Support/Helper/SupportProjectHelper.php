<?php

namespace Support\Helper;

use Craft\Support_ProjectModel;

class SupportProjectHelper
{
    public static function populateProjectModel(Support_ProjectModel $project, $data)
    {
        if (isset($data['customerId']))
        {
            $customerId = $data['customerId'];
            $project->customerId  = (is_array($customerId) && count($customerId) > 0) ? $customerId[0] : $customerId;
        }

        if (isset($data['enabled']))
        {
            $project->enabled = $data['enabled'];
        }

        if (isset($data['name']))
        {
            $project->name = $data['name'];
        }

        if (isset($data['favorite']))
        {
            $project->favorite = $data['favorite'];
        }

        if (isset($data['code']))
        {
            $project->code = $data['code'];
        }
    }
}
