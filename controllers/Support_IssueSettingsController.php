<?php

namespace Craft;


/**
 * Class Support_IssueSettingsController
 *
 * @package Craft
 */
class Support_IssueSettingsController extends Support_BaseAdminController
{
    /**
     * Edit the issue settings.
     *
     * @param array $variables
     * @throws HttpException
     * @throws Exception
     */
    public function actionEdit(array $variables = [])
    {
        $variables['issueSettings'] = craft()->support_issueSettings->getIssueSettingByHandle('issue');

        $variables['title'] = Craft::t('Issue Settings');

        $this->renderTemplate('support/settings/issuesettings/_edit', $variables);
    }

    /**
     * Saves the issue settings.
     *
     * @throws HttpException
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $issueSettings = new Support_IssueSettingsModel();

        // Shared attributes
        $issueSettings->id = craft()->request->getPost('issueSettingsId');
        $issueSettings->name = 'Issue';
        $issueSettings->handle = 'issue';

        // Set the field layout
        $fieldLayout = craft()->fields->assembleLayoutFromPost();
        $fieldLayout->type = 'Support_Issue';
        $issueSettings->setFieldLayout($fieldLayout);

        // Save it
        if (craft()->support_issueSettings->saveIssueSetting($issueSettings))
        {
            craft()->userSession->setNotice(Craft::t('Issue settings saved.'));
        }
        else
        {
            craft()->userSession->setError(Craft::t('Couldn’t save issue settings'));
        }

        craft()->urlManager->setRouteVariables(['issueSettings' => $issueSettings]);
    }
}
