<?php

namespace Craft;

use Support\Helper\SupportIssueHelper;
use Support\IssueStatus;

class Support_IssuesController extends Support_BaseCpController
{
    public function init()
    {
        craft()->userSession->requirePermission('support-manageIssues');
        parent::init();
    }

    /**
     * Issue index
     *
     * @param array $variables
     */
    public function actionIndex(array $variables = [])
    {
        $variables['projects'] = craft()->support_projects->getProjectList();

        $this->renderTemplate('support/issues/_index', $variables);
    }

    /**
     * Edit an issue.
     *
     * @param array $variables
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        $variables['issueSettings'] = craft()->support_issueSettings->getIssueSettingByHandle('issue');

        if (! $variables['issueSettings'])
        {
            throw new HttpException(404, Craft::t('No issue settings found'));
        }

        if (empty($variables['issue']))
        {
            if (! empty($variables['issueId']))
            {
                $variables['issue'] = craft()->support_issues->getIssueById($variables['issueId']);

                if (! $variables['issue'])
                {
                    throw new HttpException(404);
                }
            }
            else
            {
                $variables['issue'] = new Support_IssueModel();

                // Assign the default issue type
                $defaultIssueType = craft()->support_issueTypes->getDefaultIssueType();
                if ($defaultIssueType)
                {
                    $variables['issue']->issueTypeId = $defaultIssueType->id;
                }
            }
        }

        if (! empty($variables['issue']->id))
        {
            $variables['title'] = 'Issue ' . $variables['issue']->title;
        }
        else
        {
            $variables['title'] = Craft::t('Create a new issue');
        }

        $variables['continueEditingUrl'] = 'support/issues/{id}';

        $variables['issueStatuses'] = craft()->support_issueStatuses->getOptionList();
        $variables['projects'] = [0 => Craft::t('Select a project')] + craft()->support_projects->getProjectList();
        $variables['issueTypes'] = craft()->support_issueTypes->getOptionList();

        $this->prepVariables($variables);

        $this->renderTemplate('support/issues/_edit', $variables);
    }

    /**
     * Saves an issue.
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $issue = $this->setIssueFromPost();

        if (craft()->support_issues->saveIssue($issue))
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson([
                    'success' => true,
                    'cpEditUrl' => $issue->getCpEditUrl()
                ]);
            }
            else
            {
                $this->redirectToPostedUrl($issue);
            }
        }
        else
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => false, 'errors' => $issue->getErrors()]);
            }
            else
            {
                craft()->userSession->setError(Craft::t("Couldn‘t save issue."));
                craft()->urlManager->setRouteVariables([
                    'issue' => $issue,
                ]);
            }
        }
    }

    /**
     * Deletes an issue.
     */
    public function actionDelete()
    {
        $this->requirePostRequest();

        $issueId = craft()->request->getRequiredPost('issueId');
        $issue = craft()->support_issues->getIssueById($issueId);

        if (! $issue)
        {
            throw new Exception(Craft::t('No issue with the ID “{id}”', [
                'id' => $issueId,
            ]));
        }

        if (craft()->support_issues->delete($issue))
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => true]);
            }
            else
            {
                craft()->userSession->setNotice(Craft::t('Issue deleted.'));
                $this->redirectToPostedUrl($issue);
            }
        }
        else
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => false]);
            }
            else
            {
                craft()->userSession->setError(Craft::t('Couldn‘t delete issue.'));
                craft()->urlManager->setRouteVariables(['issue' => $issue]);
            }
        }
    }

    /**
     * Updated the issue status by using a status select modal.
     */
    public function actionUpdateStatus()
    {
        $this->requireAjaxRequest();

        $issueId = craft()->request->getParam('issueId');
        $issueStatusId = craft()->request->getParam('issueStatusId');

        $issue = craft()->support_issues->getIssueById($issueId);
        $issueStatus = craft()->support_issueStatuses->getIssueStatusById($issueStatusId);

        if (! $issue || ! $issueStatus)
        {
            $this->returnErrorJson(Craft::t('Bad issue or status'));
        }

        $issue->issueStatusId = $issueStatus->id;

        if (craft()->support_issues->saveIssue($issue))
        {
            $this->returnJson(['success' => true]);
        }
    }



    // Private Methods
    // =========================================================================


    /**
     * Modifies the variables of the request.
     *
     * @param $variables
     */
    private function prepVariables(&$variables)
    {
        $variables['tabs'] = [];

        $tabs = $variables['issueSettings']->getFieldLayout()->getTabs();

        // Hide the tabs if there is only one of them
        if (is_array($tabs) && count($tabs) < 2)
        {
            return;
        }

        foreach ($variables['issueSettings']->getFieldLayout()->getTabs() as $index => $tab)
        {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['issue']->hasErrors())
            {
                foreach ($tab->getFields() as $field)
                {
                    if ($variables['issue']->getErrors($field->getField()->handle))
                    {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'][] = [
                'label' => Craft::t($tab->name),
                'url'   => '#tab' . ($index + 1),
                'class' => ($hasErrors ? 'error' : null),
            ];
        }
    }

    /**
     * @return BaseElementModel|Support_IssueModel|null
     *
     * @throws Exception
     */
    private function setIssueFromPost()
    {
        $issueId = craft()->request->getPost('issueId');

        if ($issueId)
        {
            $issue = craft()->support_issues->getIssueById($issueId);

            if (! $issue)
            {
                throw new Exception(Craft::t('No issue with the ID “{id}”', [
                    'id' => $issueId,
                ]));
            }
        }
        else
        {
            $issue = new Support_IssueModel();
        }

        SupportIssueHelper::populateIssueModel($issue, craft()->request->getPost());

        $issue->getContent()->title = craft()->request->getPost('title', $issue->title);
        $issue->setContentFromPost('fields');

        return isset($issue) ? $issue : null;
    }

    /**
     * @param Support_IssueModel $issue
     */
    private function setContentFromPost($issue)
    {
        if (!$issue)
        {
            return;
        }

        $projectId = craft()->request->getPost('projectId', $issue->projectId);

        $issue->projectId = (is_array($projectId) && count($projectId) > 0) ? $projectId[0] : $projectId;
        $issue->issueTypeId = craft()->request->getPost('issueTypeId', $issue->issueTypeId);
        $issue->issueStatusId = craft()->request->getPost('issueStatusId', $issue->issueStatusId);
        $issue->description = craft()->request->getPost('description', $issue->description);
        $issue->solution = craft()->request->getPost('solution', $issue->solution);
    }
}
