<?php

namespace Craft;

class Support_BaseCpController extends BaseController
{
    protected $allowAnonymous = false;

    // Public Methods
    // =========================================================================

    /**
     * @inheritDoc BaseController::init()
     *
     * @throws HttpException
     */
    public function init()
    {
        craft()->userSession->requirePermission('accessPlugin-support');
    }
}
