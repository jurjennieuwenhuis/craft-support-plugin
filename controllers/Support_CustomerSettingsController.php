<?php

namespace Craft;

/**
 * Class Support_CustomerSettingsController
 *
 * Customer Setting controller.
 *
 * @package Craft
 */
class Support_CustomerSettingsController extends Support_BaseAdminController
{
    /**
     * Edit the customer settings.
     *
     * @param array $variables
     * @throws HttpException
     * @throws Exception
     */
    public function actionEdit(array $variables = [])
    {
        $variables['customerSettings'] = craft()->support_customerSettings->getCustomerSettingByHandle('customer');

        $variables['title'] = Craft::t('Customer Settings');

        $this->renderTemplate('support/settings/customersettings/_edit', $variables);
    }

    /**
     * Saves the customer settings.
     *
     * @throws HttpException
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $customerSettings = new Support_CustomerSettingsModel();

        // Shared attributes
        $customerSettings->id = craft()->request->getPost('customerSettingsId');
        $customerSettings->name = 'Customer';
        $customerSettings->handle = 'customer';

        // Set the field layout
        $fieldLayout = craft()->fields->assembleLayoutFromPost();
        $fieldLayout->type = 'Support_Customer';
        $customerSettings->setFieldLayout($fieldLayout);

        // Save it
        if (craft()->support_customerSettings->saveCustomerSetting($customerSettings))
        {
            craft()->userSession->setNotice(Craft::t('Customer settings saved.'));
        }
        else
        {
            craft()->userSession->setError(Craft::t('Couldn’t save customer settings'));
        }

        craft()->urlManager->setRouteVariables(['customerSettings' => $customerSettings]);
    }
}
