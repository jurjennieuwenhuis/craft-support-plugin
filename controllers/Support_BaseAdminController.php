<?php

namespace Craft;

/**
 * Class Support_BaseAdminController
 *
 * @package Craft
 */
class Support_BaseAdminController extends BaseController
{
    /**
     * @inheritDoc
     */
    protected $allowAnonymous = false;

    /**
     * Setup
     */
    public function init()
    {
        // All system setting actions require an admin
        craft()->userSession->requireAdmin();
    }
}
