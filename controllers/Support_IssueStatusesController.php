<?php

namespace Craft;

class Support_IssueStatusesController extends Support_BaseAdminController
{
    /**
     * @param array $variables
     */
    public function actionIndex(array $variables = [])
    {
        $variables['issueStatuses'] = craft()->support_issueStatuses->getAllIssueStatuses();

        $this->renderTemplate('support/settings/issuestatuses/index', $variables);
    }

    /**
     * @param array $variables
     *
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        if (empty($variables['issueStatus']))
        {
            if (! empty($variables['id']))
            {
                $variables['issueStatus'] = craft()->support_issueStatuses->getIssueStatusById($variables['id']);
                $variables['issueStatusId'] = $variables['issueStatus'];

                if (! $variables['issueStatus'])
                {
                    throw new HttpException(404);
                }
            }
            else
            {
                $variables['issueStatus'] = new Support_IssueStatusModel();
            }
        }

        if (! empty($variables['orderStatusId']))
        {
            $variables['title'] = $variables['orderStatus']->name;
        }
        else
        {
            $variables['title'] = Craft::t('Create a new issue status');
        }

        $this->renderTemplate('support/settings/issuestatuses/_edit', $variables);
    }

    /**
     * Save issue status.
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $model = new Support_IssueStatusModel();

        // Shared attributes
        $model->id = craft()->request->getPost('issueStatusId');
        $model->name = craft()->request->getPost('name');
        $model->handle = craft()->request->getPost('handle');
        $model->color = craft()->request->getPost('color');
        $model->default = craft()->request->getPost('default');
        $model->closed = craft()->request->getPost('closed');

        // Save it
        if (craft()->support_issueStatuses->saveIssueStatus($model))
        {
            craft()->userSession->setNotice(Craft::t('Issue status saved.'));

            $this->redirectToPostedUrl($model);
        }
        else
        {
            craft()->userSession->setError(Craft::t('Couldn’t save issue status.'));
        }

        craft()->urlManager->setRouteVariables(compact('issueStatus'));
    }

    /**
     * @return \HttpResponse
     * @throws HttpException
     */
    public function actionReorder()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $ids = JsonHelper::decode(craft()->request->getRequiredPost('ids'));

        if ($success = craft()->support_issueStatuses->reorderIssueStatuses($ids))
        {
            return $this->returnJson(['success' => $success]);
        }

        return $this->returnJson(['error' => Craft::t('Couldn\'t reorder Issue Statuses.')]);
    }

    /**
     * @throws HttpException
     */
    public function actionDelete()
    {
        $this->requireAjaxRequest();

        $issueStatusId = craft()->request->getRequiredPost('id');

        if (craft()->support_issueStatuses->deleteIssueStatusById($issueStatusId))
        {
            $this->returnJson(['success' => true]);
        }
    }
}
