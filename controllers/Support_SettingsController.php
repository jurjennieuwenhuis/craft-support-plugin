<?php

namespace Craft;

/**
 * Class Support_SettingsController
 *
 * @package Craft
 */
class Support_SettingsController extends Support_BaseAdminController
{
    /**
     * Support Settings Index
     */
    public function actionIndex()
    {
        $this->redirect('support/settings/general');
    }

    /**
     * Support Settings Form
     */
    public function actionEdit()
    {
        $settings = craft()->support_settings->getSettings();

        $this->renderTemplate('support/settings/general',[
            'settings' => $settings,
            'assetSources' => [0 => 'Select an asset source'] + craft()->support_settings->getAssetSources(),
        ]);
    }

    /**
     * @throws HttpException
     */
    public function actionSaveSettings()
    {
        $this->requirePostRequest();

        $postData = craft()->request->getPost('settings');
        $settings = Support_SettingsModel::populateModel($postData);

        if (! craft()->support_settings->saveSettings($settings))
        {
            craft()->userSession->setError(Craft::t('Couldn’t save settings.'));
            $this->renderTemplate('support/settings', ['settings' => $settings]);
        }
        else
        {
            craft()->userSession->setNotice(Craft::t('Settings saved.'));
            $this->redirectToPostedUrl();
        }
    }
}
