<?php

namespace Craft;

use Support\Helper\SupportCustomerHelper;

class Support_CustomersController extends Support_BaseCpController
{
    // Public Methods
    // =========================================================================


    public function init()
    {
        craft()->userSession->requirePermission('support-manageCustomers');

        parent::init();
    }

    /**
     * Customer index
     */
    public function actionIndex()
    {
        $this->renderTemplate('support/customers/_index');
    }

    /**
     * Edit an customer.
     *
     * @param array $variables
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        $variables['customerSettings'] = craft()->support_customerSettings->getCustomerSettingByHandle('customer');

        if (! $variables['customerSettings'])
        {
            throw new HttpException(404, Craft::t('No customer settings found.'));
        }

        // Now let's set up the actual customer
        if (empty($variables['customer']))
        {
            if (! empty($variables['customerId']))
            {
                $variables['customer'] = craft()->support_customers->getCustomerById($variables['customerId']);

                if (! $variables['customer'])
                {
                    throw new HttpException(404);
                }
            }
            else
            {
                $variables['customer'] = new Support_CustomerModel();
            }
        }

        if (! empty($variables['customer']->id))
        {
            $variables['title'] = 'Customer ' . $variables['customer']->name;
        }
        else
        {
            $variables['title'] = Craft::t('Create a new customer');
        }

        $variables['continueEditingUrl'] = 'support/customers/{id}';

        $this->prepVariables($variables);

        // Render the template!
        $this->renderTemplate('support/customers/_edit', $variables);
    }

    /**
     * Saves a new or existing customer.
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $customer = $this->setCustomerFromPost();

        if (craft()->support_customers->saveCustomer($customer))
        {
            $this->redirectToPostedUrl($customer);
        }

        craft()->userSession->setError(Craft::t('Couldn’t save customer.'));
        craft()->urlManager->setRouteVariables([
            'customer' => $customer,
        ]);
    }

    /**
     * Deletes an customer.
     */
    public function actionDelete()
    {
        $this->requirePostRequest();

        $customerId = craft()->request->getRequiredPost('customerId');
        $customer = craft()->support_customers->getCustomerById($customerId);

        if (! $customer)
        {
            throw new Exception(Craft::t('No customer with the ID “{id}”.', [
                'id' => $customerId,
            ]));
        }

        if (craft()->support_customers->deleteCustomer($customer))
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => true]);
            }
            else
            {
                craft()->userSession->setNotice(Craft::t('Customer deleted'));
                $this->redirectToPostedUrl($customer);
            }
        }
        else
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => false]);
            }
            else
            {
                craft()->userSession->setError(Craft::t('Couldn‘t delete customer.'));
                craft()->urlManager->setRouteVariables(['customer' => $customer]);
            }
        }
    }


    // Private Methods
    // =========================================================================


    /**
     * Modifies the variables of the request.
     *
     * @param $variables
     */
    private function prepVariables(&$variables)
    {
        $variables['tabs'] = [];

        // Hide the tabs if there is only one of them
        $tabs = $variables['customerSettings']->getFieldLayout()->getTabs();

        if (is_array($tabs) && count($tabs) < 2)
        {
            return;
        }

        foreach ($tabs as $index => $tab)
        {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['customer']->hasErrors())
            {
                foreach ($tab->getFields() as $field)
                {
                    if ($variables['customer']->getErrors($field->getField()->handle))
                    {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'][] = [
                'label' => Craft::t($tab->name),
                'url'   => '#tab' . ($index + 1),
                'class' => ($hasErrors ? 'error' : null),
            ];
        }
    }

    /**
     * @return BaseElementModel|Support_CustomerModel|null
     *
     * @throws Exception
     */
    private function setCustomerFromPost()
    {
        $customerId = craft()->request->getPost('customerId');

        if ($customerId)
        {
            $customer = craft()->support_customers->getCustomerById($customerId);

            if (! $customer)
            {
                throw new Exception(Craft::t('No customer with the ID “{id}”.', [
                    'id' => $customerId,
                ]));
            }
        }
        else
        {
            $customer = new Support_CustomerModel();
        }

        SupportCustomerHelper::populateCustomerModel($customer, craft()->request->getPost());
        $customer->setContentFromPost('fields');

        return isset($customer) ? $customer : null;
    }

    /**
     * @param Support_CustomerModel $customer
     */
    private function setContentFromPost($customer)
    {
        if (!$customer)
        {
            return;
        }
        $customer->name = craft()->request->getPost('name', $customer->name);
        $customer->code = craft()->request->getPost('code', $customer->code);
        $customer->setContentFromPost('fields');
    }
}
