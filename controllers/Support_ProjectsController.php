<?php

namespace Craft;

use Support\Helper\SupportProjectHelper;

class Support_ProjectsController extends Support_BaseCpController
{
    // Public Methods
    // =========================================================================


    public function init()
    {
        craft()->userSession->requirePermission('support-manageProjects');

        parent::init();
    }

    /**
     * Project index
     */
    public function actionIndex()
    {
        $variables['customers'] = craft()->support_customers->getCustomerList();

        $this->renderTemplate('support/projects/_index', $variables);
    }

    /**
     * Edit an project.
     *
     * @param array $variables
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        $variables['projectSettings'] = craft()->support_projectSettings->getProjectSettingByHandle('project');

        if (! $variables['projectSettings'])
        {
            throw new HttpException(404, Craft::t('No project settings found.'));
        }

        // Now let's set up the actual project
        if (empty($variables['project']))
        {
            if (! empty($variables['projectId']))
            {
                $variables['project'] = craft()->support_projects->getProjectById($variables['projectId']);

                if (! $variables['project'])
                {
                    throw new HttpException(404);
                }
            }
            else
            {
                $variables['project'] = new Support_ProjectModel();
            }
        }

        if (! empty($variables['project']->id))
        {
            $variables['title'] = 'Project ' . $variables['project']->name;
        }
        else
        {
            $variables['title'] = Craft::t('Create a new project');
        }

        $variables['continueEditingUrl'] = 'support/projects/{id}';

        $variables['customers'] = [0 => Craft::t('Select a customer')] + craft()->support_customers->getCustomerList();

        $this->prepVariables($variables);

        // Render the template!
        $this->renderTemplate('support/projects/_edit', $variables);
    }

    /**
     * Saves a new or existing project.
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $project = $this->setProjectFromPost();

        if (craft()->support_projects->saveProject($project))
        {
            $this->redirectToPostedUrl($project);
        }

        craft()->userSession->setError(Craft::t('Couldn’t save project.'));
        craft()->urlManager->setRouteVariables([
            'project' => $project,
        ]);
    }

    /**
     * Deletes an project.
     */
    public function actionDelete()
    {
        $this->requirePostRequest();

        $projectId = craft()->request->getRequiredPost('projectId');
        $project = craft()->support_projects->getProjectById($projectId);

        if (! $project)
        {
            throw new Exception(Craft::t('No project with the ID “{id}”.', [
                'id' => $projectId,
            ]));
        }

        if (craft()->support_projects->deleteProject($project))
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => true]);
            }
            else
            {
                craft()->userSession->setNotice(Craft::t('Project deleted'));
                $this->redirectToPostedUrl($project);
            }
        }
        else
        {
            if (craft()->request->isAjaxRequest())
            {
                $this->returnJson(['success' => false]);
            }
            else
            {
                craft()->userSession->setError(Craft::t('Couldn‘t delete project.'));
                craft()->urlManager->setRouteVariables(['project' => $project]);
            }
        }
    }


    // Private Methods
    // =========================================================================


    /**
     * Modifies the variables of the request.
     *
     * @param $variables
     */
    private function prepVariables(&$variables)
    {
        $variables['tabs'] = [];

        $tabs = $variables['projectSettings']->getFieldLayout()->getTabs();

        // Hide the tabs if there is only one of them
        if (is_array($tabs) && count($tabs) < 2)
        {
            return;
        }

        foreach ($variables['projectSettings']->getFieldLayout()->getTabs() as $index => $tab)
        {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['project']->hasErrors())
            {
                foreach ($tab->getFields() as $field)
                {
                    if ($variables['project']->getErrors($field->getField()->handle))
                    {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'][] = [
                'label' => Craft::t($tab->name),
                'url'   => '#tab' . ($index + 1),
                'class' => ($hasErrors ? 'error' : null),
            ];
        }
    }

    /**
     * @return BaseElementModel|Support_ProjectModel|null
     *
     * @throws Exception
     */
    private function setProjectFromPost()
    {
        $projectId = craft()->request->getPost('projectId');

        if ($projectId)
        {
            $project = craft()->support_projects->getProjectById($projectId);

            if (! $project)
            {
                throw new Exception(Craft::t('No project with the ID “{id}”.', [
                    'id' => $projectId,
                ]));
            }
        }
        else
        {
            $project = new Support_ProjectModel();
        }

        SupportProjectHelper::populateProjectModel($project, craft()->request->getPost());

        $project->setContentFromPost('fields');

        return isset($project) ? $project : null;
    }

    /**
     * @param Support_ProjectModel $project
     */
    private function setContentFromPost($project)
    {
        if (!$project)
        {
            return;
        }
        $project->name = craft()->request->getPost('name', $project->name);
        $project->code = craft()->request->getPost('code', $project->code);
        $project->favorite = craft()->request->getPost('favorite', $project->favorite);
        $project->customerId = craft()->request->getPost('customerId', $project->customerId);
        $project->enabled = craft()->request->getPost('enabled', $project->enabled);
        $project->setContentFromPost('fields');
    }
}
