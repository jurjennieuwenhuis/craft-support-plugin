<?php

namespace Craft;

class Support_IssueTypesController extends Support_BaseAdminController
{
    /**
     * @param array $variables
     */
    public function actionIndex(array $variables = [])
    {
        $variables['issueTypes'] = craft()->support_issueTypes->getAllIssueTypes();

        $this->renderTemplate('support/settings/issuetypes/index', $variables);
    }

    /**
     * @param array $variables
     *
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        if (empty($variables['issueType']))
        {
            if (! empty($variables['id']))
            {
                $variables['issueType'] = craft()->support_issueTypes->getIssueTypeById($variables['id']);
                $variables['issueTypeId'] = $variables['issueType'];

                if (! $variables['issueType'])
                {
                    throw new HttpException(404);
                }
            }
            else
            {
                $variables['issueType'] = new Support_IssueTypeModel();
            }
        }

        if (! empty($variables['issueTypeId']))
        {
            $variables['title'] = $variables['issuetype']->name;
        }
        else
        {
            $variables['title'] = Craft::t('Create a new issue type');
        }

        $this->renderTemplate('support/settings/issuetypes/_edit', $variables);
    }

    /**
     * Save issue type.
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $issueType = new Support_IssueTypeModel();

        // Shared attributes
        $issueType->id = craft()->request->getPost('issueTypeId');
        $issueType->name = craft()->request->getPost('name');
        $issueType->handle = craft()->request->getPost('handle');
        $issueType->default = craft()->request->getPost('default');

        // Save it
        if (craft()->support_issueTypes->saveIssueType($issueType))
        {
            craft()->userSession->setNotice(Craft::t('Issue type saved.'));

            $this->redirectToPostedUrl($issueType);
        }
        else
        {
            craft()->userSession->setError(Craft::t('Couldn’t save issue type.'));
        }

        craft()->urlManager->setRouteVariables(compact('issueType'));
    }

    /**
     * @return \HttpResponse
     * @throws HttpException
     */
    public function actionReorder()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $ids = JsonHelper::decode(craft()->request->getRequiredPost('ids'));

        if ($success = craft()->support_issueTypes->reorderIssueTypes($ids))
        {
            return $this->returnJson(['success' => $success]);
        }

        return $this->returnJson(['error' => Craft::t('Couldn\'t reorder Issue Types.')]);
    }

    /**
     * @throws HttpException
     */
    public function actionDelete()
    {
        $this->requireAjaxRequest();

        $issueTypeId = craft()->request->getRequiredPost('id');

        if (craft()->support_issueTypes->deleteIssueTypeById($issueTypeId))
        {
            $this->returnJson(['success' => true]);
        }
    }
}
