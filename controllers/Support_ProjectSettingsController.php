<?php

namespace Craft;

/**
 * Class Support_ProjectSettingsController
 *
 * Project Setting controller.
 *
 * @package Craft
 */
class Support_ProjectSettingsController extends Support_BaseAdminController
{
    /**
     * Edit the project settings.
     *
     * @param array $variables
     * @throws HttpException
     * @throws Exception
     */
    public function actionEdit(array $variables = [])
    {
        $variables['projectSettings'] = craft()->support_projectSettings->getProjectSettingByHandle('project');

        $variables['title'] = Craft::t('Project Settings');

        $this->renderTemplate('support/settings/projectsettings/_edit', $variables);
    }

    /**
     * Saves the customer settings.
     *
     * @throws HttpException
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        $projectSettings = new Support_ProjectSettingsModel();

        // Shared attributes
        $projectSettings->id = craft()->request->getPost('projectSettingsId');
        $projectSettings->name = 'Project';
        $projectSettings->handle = 'project';

        // Set the field layout
        $fieldLayout = craft()->fields->assembleLayoutFromPost();
        $fieldLayout->type = 'Support_Project';
        $projectSettings->setFieldLayout($fieldLayout);

        // Save it
        if (craft()->support_projectSettings->saveProjectSetting($projectSettings))
        {
            craft()->userSession->setNotice(Craft::t('Project settings saved.'));
        }
        else
        {
            craft()->userSession->setError(Craft::t('Couldn’t save project settings'));
        }

        craft()->urlManager->setRouteVariables(['projectSettings' => $projectSettings]);
    }
}
