<?php

return [
    // Issues
    'support/issues'                            => ['action' => 'support/issues/index'],
    'support/issues/new'                        => ['action' => 'support/issues/edit'],
    'support/issues/(?P<issueId>\d+)'           => ['action' => 'support/issues/edit'],

    // Customers
    'support/customers'                         => ['action' => 'support/customers/index'],
    'support/customers/new'                     => ['action' => 'support/customers/edit'],
    'support/customers/(?P<customerId>\d+)'     => ['action' => 'support/customers/edit'],

    // Projects
    'support/projects'                          => ['action' => 'support/projects/index'],
    'support/projects/new'                      => ['action' => 'support/projects/edit'],
    'support/projects/(?P<projectId>\d+)'       => ['action' => 'support/projects/edit'],

    // Settings
    'support/settings/general'                  => ['action' => 'support/settings/edit'],
    'support/settings'                          => ['action' => 'support/settings/index'],

    // Issue Field Layout
    'support/settings/issuesettings'            => ['action' => 'support/issueSettings/edit'],

    // Customer Field Layout
    'support/settings/customersettings'         => ['action' => 'support/customerSettings/edit'],

    // Project Field Layout
    'support/settings/projectsettings'          => ['action' => 'support/projectSettings/edit'],

    // Issue Types
    'support/settings/issuetypes'               => ['action' => 'support/issueTypes/index'],
    'support/settings/issuetypes/new'           => ['action' => 'support/issueTypes/edit'],
    'support/settings/issuetypes/(?P<id>\d+)'   => ['action' => 'support/issueTypes/edit'],

    // Issue Types
    'support/settings/issuestatuses'               => ['action' => 'support/issueStatuses/index'],
    'support/settings/issuestatuses/new'           => ['action' => 'support/issueStatuses/edit'],
    'support/settings/issuestatuses/(?P<id>\d+)'   => ['action' => 'support/issueStatuses/edit'],

    // Support
    'support'                                   => ['action' => 'support/issues/index'],
];
